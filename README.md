# DashDroid - Dashcam on Android with driver assistance (18CS036)
*Driving Assistance Systems* (**DAS**) can be helpful during driving, and helps to prevent potential traffic accidents by complementing drivers’ awareness on the road situation, but such a system is not available on all vehicles. On the other hand, dashcams have received widespread use by different vehicle owners. As dashcams, along with built in sensors of a mobile device can on theory provide sufficient data for DAS like functionality , an attempt is made to implement certain functionality commonly seen on DASs onto an Android device, thus making such features available to a certain extent, to most users with an Android device,  independent to vehicle types and models they may drive.

Imagery data is captured using the device's camera, which is then processed by a *Convolution Neural Network*(**CNN**) to obtain additional data. Such data, along with data obtained from the device's orientation sensor and GPS locator, is used to provide the following drive assistance feature:
-  Forward collision warning
-  Lane departure warning
-  Traffic light notification and warning
-  Low illumination warning
-  Safety distance warning
-  Speed limit detection and speeding warning

The application was tested on an actual vehicle in a real traffic scenario, and was shown to be able to provide basic drive assistance functionalities listed above to the user. Future development of the application, if any, should be focused on improving the inconsistencies in the info that the application may give to the user and to improve on the quality of currently provided functionalities. 

## Setup
To run the application, import the repo as gradle project in *Android Studio* / *IntelliJ IDEA*, and run like normal Android application.