/*
 * Copyright (c) 2019. Yip Chung Nin
 * Developed as part of project 18CS036 (DashDroid - Dash Cam with driver assistance) at City University of Hong Kong
 * All rights reserved
 */

package p18cs036.dashdroid.computervision.processors;

import android.util.Log;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import java.util.Arrays;

import p18cs036.dashdroid.ConcurrentKeyValueMap;
import p18cs036.dashdroid.Utils;
import p18cs036.dashdroid.computervision.ImageProcessingManager;
import p18cs036.dashdroid.dashcam.DashCam;

import static p18cs036.dashdroid.computervision.processors.LaneStateDetector.forwardAreaCoordinates;

public class IlluminationFinder extends ImageProcessor {
	private final String LOG_TAG = this.getClass().getName();
	public static final String KEY = "LUMA";
	public static final String KEY_UPPER = "LUMA_UP";
	public static final String KEY_LOWER = "LUMA_LOW";
	public static final String KEY_ILLUMINATION = "NIGHT";
	public static final String KEY_HEADLIGHTS_ENABLED = "HEADLIGHTS_ENABLED";

	private static final int EXPOSURE_DAY_NIGHT_THRESHOLD = 100;
	private static final int IMG_LUMA_DAY_NIGHT_THRESHOLD = 70;

	@Override
	public String getKey() {
		return KEY;
	}

	@Override
	long getMinUpdateInterval() {
		return (1 * 60 * 1000);//5 minutes
	}

	@Override
	Runnable process() {
		return new Runnable() {
			@Override
			public void run() {
				Mat grey = new Mat();
				Imgproc.cvtColor(processingFrame, grey, Imgproc.COLOR_RGB2GRAY);
				ConcurrentKeyValueMap map = resultsMap;
				int horizon = map.getInt(HorizonFinder.KEY, HorizonFinder.DEFAULT_HORIZON);
				horizon = Utils.clampValueToInt(horizon, 1, ImageProcessingManager.FRAME_HEIGHT - 1);
				Mat upper = grey.submat(0, horizon, 0, grey.cols());
				Mat lower = grey.submat(horizon, grey.rows(), 0, grey.cols());
				double avgLum = Core.mean(grey).val[0];
				double avgLumUp = Core.mean(upper).val[0];
				double avgLumLow = Core.mean(lower).val[0];
				Log.v(LOG_TAG, "Overall luminosity= " + avgLum);
				Log.v(LOG_TAG, "Upper half luminosity= " + avgLumUp);
				Log.v(LOG_TAG, "Lower luminosity= " + avgLumLow);
				map.putDouble(KEY, avgLum);
				map.putDouble(KEY_UPPER, avgLumUp);
				map.putDouble(KEY_LOWER, avgLumLow);
				map.putBoolean(KEY_ILLUMINATION, isNight(map.getInt(DashCam.DATA_KEY_CAM_EXPOSURE), avgLum));
				map.putBoolean(KEY_HEADLIGHTS_ENABLED, isHeadlightsOn(grey, avgLumLow, horizon));
			}
		};
	}

	private boolean isNight(int exp, double avgLum) {
		return exp > EXPOSURE_DAY_NIGHT_THRESHOLD && avgLum < IMG_LUMA_DAY_NIGHT_THRESHOLD;
	}

	private boolean isHeadlightsOn(Mat image, double lowerLum, int horizon) {
		Log.d(LOG_TAG, Arrays.toString(forwardAreaCoordinates));
		Mat forward = image.submat(horizon, ImageProcessingManager.FRAME_HEIGHT, Utils.clampValueToInt(forwardAreaCoordinates[2].x, 0, ImageProcessingManager.FRAME_WIDTH), Utils.clampValueToInt(forwardAreaCoordinates[3].x, 0, ImageProcessingManager.FRAME_WIDTH));
		return Core.mean(forward).val[0] > lowerLum;
	}
}
