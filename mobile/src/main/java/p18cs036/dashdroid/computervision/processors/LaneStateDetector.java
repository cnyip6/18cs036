/*
 * Copyright (c) 2019. Yip Chung Nin
 * Developed as part of project 18CS036 (DashDroid - Dash Cam with driver assistance) at City University of Hong Kong
 * All rights reserved
 */

package p18cs036.dashdroid.computervision.processors;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import p18cs036.dashdroid.ConcurrentKeyValueMap;
import p18cs036.dashdroid.computervision.ImageProcessingManager;
import p18cs036.dashdroid.dashcam.DashCam;
import p18cs036.dashdroid.dashcam.DataRecorder;

public class LaneStateDetector extends ImageProcessor {
	private final String LOG_TAG = this.getClass().getName();
	public static final String KEY = "LANE_DEPARTURE";

	public enum LaneDepartureStatus {
		IN_LANE,
		OFF_LANE_LEFT,
		OFF_LANE_RIGHT,
		NO_LANE
	}

	public static final double DETECT_RANGE = 10;//in meters
	public static final double MIN_DETECT_RANGE = 3;
	public static final double LANE_WIDTH = 3;//Lane width in meters

	//initial configs, will be overridden with generated value during runtime
	private static final double DETECT_LENGTH = 0.4;//How far ahead to detect(initial value). units are proportion realative to the vertical height of the image.
	private static final double DEFAULT_DETECTION_ZONE_WIDTH_FORWARD_BASE = 0.6;//magic
	private static final double LANE_CONVERGENCE_FACTOR = 1.0 / 3.0;
	private static final double DEFAULT_DETECTION_ZONE_WIDTH_FORWARD_HORIZON = DEFAULT_DETECTION_ZONE_WIDTH_FORWARD_BASE + (DEFAULT_DETECTION_ZONE_WIDTH_FORWARD_BASE * LANE_CONVERGENCE_FACTOR - DEFAULT_DETECTION_ZONE_WIDTH_FORWARD_BASE) / 0.3 * DETECT_LENGTH;//magic

	private double forwardAreaBaseWidth;
	private double forwardAreaForwardWidth;

	public static Point[] forwardAreaCoordinates = {//TODO change to private and dump into map?
	                                                new Point(ImageProcessingManager.FRAME_WIDTH * (1 - DEFAULT_DETECTION_ZONE_WIDTH_FORWARD_HORIZON) * 0.5, (1 - DETECT_LENGTH) * ImageProcessingManager.FRAME_HEIGHT),
	                                                new Point(ImageProcessingManager.FRAME_WIDTH * (1 + DEFAULT_DETECTION_ZONE_WIDTH_FORWARD_HORIZON) * 0.5, (1 - DETECT_LENGTH) * ImageProcessingManager.FRAME_HEIGHT),
	                                                new Point(ImageProcessingManager.FRAME_WIDTH * (1 - DEFAULT_DETECTION_ZONE_WIDTH_FORWARD_BASE) * 0.5, ImageProcessingManager.FRAME_HEIGHT - 1),
	                                                new Point(ImageProcessingManager.FRAME_WIDTH * (1 + DEFAULT_DETECTION_ZONE_WIDTH_FORWARD_BASE) * 0.5, ImageProcessingManager.FRAME_HEIGHT - 1)
	};

	private static final Point[] transformAreaCoordinate = {
			new Point(ImageProcessingManager.FRAME_WIDTH * 0.2, 0),
			new Point(ImageProcessingManager.FRAME_WIDTH * 0.8, 0),
			new Point(ImageProcessingManager.FRAME_WIDTH * 0.2, ImageProcessingManager.FRAME_HEIGHT - 1),
			new Point(ImageProcessingManager.FRAME_WIDTH * 0.8, ImageProcessingManager.FRAME_HEIGHT - 1)
	};

	private Mat transformMatrix;

	private static final double LANE_RECOGNITION_THRESHOLD_FACTOR = 1.5;//threshold used to distinguish lane markings from road surface, as the factor of average area intensity

	private static final double SYMMETRIC_VARIATION_TOLERANCE = 0.2;

	@Override
	public String getKey() {
		return KEY;
	}

	@Override
	long getMinUpdateInterval() {
		return 0;
	}

	@Override
	public void initialize() {
		super.initialize();
		//TODO revise transformation point
		handler.post(new Runnable() {
			@Override
			public void run() {
				forwardAreaBaseWidth = DEFAULT_DETECTION_ZONE_WIDTH_FORWARD_BASE;
				forwardAreaForwardWidth = DEFAULT_DETECTION_ZONE_WIDTH_FORWARD_HORIZON;
			}
		});
	}

	@Override
	Runnable process() {
		return new Runnable() {
			@Override
			public void run() {
				updateForwardCoordinates();
				updateTransformMatrix();
				Mat warped = new Mat();
				Mat grey = new Mat();
				Mat binary = new Mat();
				Mat histogram = new Mat();
				Imgproc.cvtColor(processingFrame, grey, Imgproc.COLOR_RGB2GRAY);
				Imgproc.warpPerspective(grey, warped, transformMatrix, new Size(ImageProcessingManager.FRAME_WIDTH, ImageProcessingManager.FRAME_HEIGHT));//transform image to see two straight lines for lane
				Imgproc.adaptiveThreshold(warped, binary, 255, Imgproc.ADAPTIVE_THRESH_MEAN_C, Imgproc.THRESH_BINARY, 251, - 40);//exclude everything but lane by converting to binary img
				Mat l = binary.submat(0, ImageProcessingManager.FRAME_HEIGHT - 1, 0, ImageProcessingManager.FRAME_WIDTH / 2 - 1);
				Mat r = binary.submat(0, ImageProcessingManager.FRAME_HEIGHT - 1, ImageProcessingManager.FRAME_WIDTH / 2, ImageProcessingManager.FRAME_WIDTH - 1);
				Core.reduce(l, histogram, 0, Core.REDUCE_SUM, CvType.CV_32F);//get distribution of white pixel (sort of)
				Imgproc.GaussianBlur(histogram, histogram, new Size(5, 5), 0, 0);
				Core.MinMaxLocResult resultL = Core.minMaxLoc(histogram);//find location of lane line in image by finding the peak for white pixel
				histogram = new Mat();
				Core.reduce(r, histogram, 0, Core.REDUCE_SUM, CvType.CV_32F);
				Imgproc.GaussianBlur(histogram, histogram, new Size(5, 5), 0, 0);
				Core.MinMaxLocResult resultR = Core.minMaxLoc(histogram);
				resultsMap.put(getKey(), isOffLane(resultL.maxLoc.x, resultR.maxLoc.x));
			}
		};
	}

	private void updateForwardCoordinates() {
		//get the supposed projection of the real life coordinates
		Point tl = getProjectedLocation(- LANE_WIDTH / 2.0, 0, DETECT_RANGE);
		Point tr = getProjectedLocation(LANE_WIDTH / 2.0, 0, DETECT_RANGE);
		Point br = getProjectedLocation(LANE_WIDTH / 2.0, 0, MIN_DETECT_RANGE);
		Point bl = getProjectedLocation(- LANE_WIDTH / 2.0, 0, MIN_DETECT_RANGE);
		//adjust all points so that they fall inside the image
		double tbrx = (br.x - tr.x) / (br.y - tr.y) * (ImageProcessingManager.FRAME_HEIGHT - tr.y) + tr.x;
		double tbry = (br.y - tr.y) / (br.x - tr.x) * (tbrx - tr.x) + tr.y;
		Point tbr = new Point(
				tbrx,
				tbry
		);
		double tblx = (tl.x - bl.x) / (tl.y - bl.y) * (ImageProcessingManager.FRAME_HEIGHT - tl.y) + tl.x;
		double tbly = (bl.y - tl.y) / (tl.x - bl.x) * (tl.x - tblx) + tr.y;
		Point tbl = new Point(
				tblx,
				tbly
		);
		forwardAreaCoordinates = new Point[]{
				tl,
				tr,
				tbl,
				tbr
		};
	}

	private void updateTransformMatrix() {
		MatOfPoint2f assumedLaneArea = new MatOfPoint2f(forwardAreaCoordinates);
		MatOfPoint2f transformedLaneArea = new MatOfPoint2f(transformAreaCoordinate);
		transformMatrix = Imgproc.getPerspectiveTransform(assumedLaneArea, transformedLaneArea);
	}

	private LaneDepartureStatus isOffLane(double leftLanePos, double rightLanePos) {
		if (leftLanePos == 0 && rightLanePos == 0) {//no lane in sight
			return LaneDepartureStatus.NO_LANE;
		} else {
			double symmetricVariation = (leftLanePos + (ImageProcessingManager.FRAME_WIDTH / 2.0) + rightLanePos) / 2 - ImageProcessingManager.FRAME_WIDTH / 2.0;
			if (Math.abs(symmetricVariation) <= ImageProcessingManager.FRAME_WIDTH * SYMMETRIC_VARIATION_TOLERANCE) {
				return LaneDepartureStatus.IN_LANE;
			} else if (symmetricVariation < 0) {
				return LaneDepartureStatus.OFF_LANE_RIGHT;
			} else if (symmetricVariation > 0) {
				return LaneDepartureStatus.OFF_LANE_LEFT;
			}
			return LaneDepartureStatus.NO_LANE;
		}
	}

	private Point getProjectedLocation(double x, double y, double z) {
		ConcurrentKeyValueMap map = ImageProcessingManager.getInstance().getResultsMap();
		double focalLengthY = map.getDouble(DashCam.DATA_KEY_CAM_FOCAL_LENGTH_Y);
		double focalLengthX = map.getDouble(DashCam.DATA_KEY_CAM_FOCAL_LENGTH_X);
		double copY = map.getDouble(DashCam.DATA_KEY_CAM_CENTER_OF_VIEW_Y);
		double copX = map.getDouble(DashCam.DATA_KEY_CAM_CENTER_OF_VIEW_X);
		double pitch = Math.toRadians(map.getDouble(HorizonFinder.KEY_PITCH, map.getDouble(DataRecorder.DATA_KEY_PITCH, 0.0)));
		double px = copX + (focalLengthX * x) / (z * Math.cos(pitch) + (- ImageProcessingManager.POV_HEIGHT + y) * Math.sin(pitch));
		double py = copY + (focalLengthY * (ImageProcessingManager.POV_HEIGHT - y * Math.cos(pitch) + z * Math.sin(pitch))) / (z * Math.cos(pitch) + (- ImageProcessingManager.POV_HEIGHT + y) * Math.sin(pitch));
		return new Point(px, py);
	}
}
