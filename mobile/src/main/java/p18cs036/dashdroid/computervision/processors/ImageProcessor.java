/*
 * Copyright (c) 2019. Yip Chung Nin
 * Developed as part of project 18CS036 (DashDroid - Dash Cam with driver assistance) at City University of Hong Kong
 * All rights reserved
 */

package p18cs036.dashdroid.computervision.processors;

import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

import org.opencv.core.Mat;

import p18cs036.dashdroid.ConcurrentKeyValueMap;
import p18cs036.dashdroid.computervision.ImageProcessingManager;

public abstract class ImageProcessor {
	private final String LOG_TAG = this.getClass().getName();

	//own thread for running
	private HandlerThread thread;
	protected Handler handler;

	protected volatile boolean readyForFrame = false;
	protected boolean enabled = true;

	protected ConcurrentKeyValueMap resultsMap;

	protected Mat processingFrame;

	//update request
	protected long lastUpdateTime;

	//benchmarking symbols
	private long startTime;
	private int frameCount;

	public abstract String getKey();

	public boolean isRequestingUpdate() {
		//return enabled && (System.currentTimeMillis() - lastUpdateTime) > getMinUpdateInterval();
		return enabled && true;//for now
	}

	abstract long getMinUpdateInterval();

	private void startThread() {
		thread = new HandlerThread("OpenCVProcessingThread");
		thread.start();
		handler = new Handler(thread.getLooper());
	}

	private void stopThread() {
		thread.quitSafely();
		try {
			thread.join();
			thread = null;
			handler = null;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void initialize() {
		startThread();
		handler.post(new Runnable() {
			@Override
			public void run() {
				resultsMap = ImageProcessingManager.getInstance().getResultsMap();
				startTime = System.currentTimeMillis();
				readyForFrame = true;
			}
		});
	}

	public void enable() {
		enabled = true;
	}

	public void disable() {
		enabled = false;
	}

	public boolean isReadyForFrame() {
		return readyForFrame;
	}

	public void processFrame(final Mat image) {
		handler.post(new Runnable() {
			@Override
			public void run() {
				readyForFrame = false;
				processingFrame = image.clone();//TODO if mem is ok, convert mat to bmp first then converting bmp to mat. use built in functions, man.
				lastUpdateTime = System.currentTimeMillis();
				Log.v(LOG_TAG, "Received mat, begin processing");
			}
		});
		handler.post(process());
		handler.post(new Runnable() {
			@Override
			public void run() {
				Log.v(LOG_TAG, "Frame processed");
				processingFrame = null;//processing complete, throw it away
				readyForFrame = true;
				frameCount++;
				ImageProcessingManager.getInstance().onResultsUpdated(getKey());
			}
		});
	}

	abstract Runnable process();

	public void stop() {
		stopThread();
		printBenchMark();
	}

	private void printBenchMark() {
		Log.d(LOG_TAG, "Processing rate: " + (double) frameCount / ((System.currentTimeMillis() - startTime) / 1000));
	}

}
