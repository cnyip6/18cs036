/*
 * Copyright (c) 2019. Yip Chung Nin
 * Developed as part of project 18CS036 (DashDroid - Dash Cam with driver assistance) at City University of Hong Kong
 * All rights reserved
 */

package p18cs036.dashdroid.computervision.processors;

import java.util.ArrayList;

import p18cs036.dashdroid.ConcurrentKeyValueMap;
import p18cs036.dashdroid.Utils;
import p18cs036.dashdroid.computervision.ImageProcessingManager;
import p18cs036.dashdroid.dashcam.DashCam;
import p18cs036.dashdroid.dashcam.DataRecorder;

public class HorizonFinder extends ImageProcessor {
	private final String LOG_TAG = this.getClass().getName();
	public static final String KEY = "HORIZON";
	public static final String KEY_PITCH = "AVG_PITCH";
	public static final int DEFAULT_HORIZON = ImageProcessingManager.FRAME_HEIGHT / 2;

	private static final int PITCH_VALUE_FILTER_SIZE = 30;

	private ArrayList<Double> pitchHistory;

	@Override
	public String getKey() {
		return KEY;
	}

	@Override
	long getMinUpdateInterval() {
		return 0;
	}

	@Override
	public void initialize() {
		super.initialize();
		handler.post(new Runnable() {
			@Override
			public void run() {
				pitchHistory = new ArrayList<>();
			}
		});
	}

	@Override
	Runnable process() {
		return new Runnable() {
			@Override
			public void run() {
				ConcurrentKeyValueMap map = resultsMap;
				double defaultHorizon = map.getDouble(DashCam.DATA_KEY_CAM_CENTER_OF_VIEW_Y);
				double focalLengthVertical = map.getDouble(DashCam.DATA_KEY_CAM_FOCAL_LENGTH_Y);
				pitchHistory.add(map.getDouble(DataRecorder.DATA_KEY_PITCH, 0.0));
				double avgPitch = Utils.simpleMovingAverageFilter(pitchHistory, PITCH_VALUE_FILTER_SIZE);
				double horizonPosition = defaultHorizon + focalLengthVertical * Math.tan(Math.toRadians(avgPitch));
				map.putDouble(KEY_PITCH, avgPitch);
				map.putInt(KEY, (int) horizonPosition);
			}
		};
	}

}