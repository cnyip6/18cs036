/*
 * Copyright (c) 2019. Yip Chung Nin
 * Developed as part of project 18CS036 (DashDroid - Dash Cam with driver assistance) at City University of Hong Kong
 * All rights reserved
 */

package p18cs036.dashdroid.computervision.processors;

import android.util.Log;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Rect2d;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.dnn.Dnn;
import org.opencv.dnn.Net;
import org.opencv.imgproc.Imgproc;
import org.opencv.utils.Converters;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import p18cs036.dashdroid.Utils;
import p18cs036.dashdroid.computervision.ImageProcessingManager;
import p18cs036.dashdroid.computervision.model.SpeedSign;
import p18cs036.dashdroid.computervision.model.TrafficLight;
import p18cs036.dashdroid.computervision.model.TrafficSign;
import p18cs036.dashdroid.computervision.model.Vehicle;

import static org.opencv.core.CvType.CV_8UC1;

public class ObjectDetector extends ImageProcessor {
	private final String LOG_TAG = this.getClass().getName();
	public static final String KEY = "OBJ_DNN";

	public static final String KEY_REF_IMAGE = "img";
	public static final String KEY_VEHICLE = "VEHICLE";
	public static final String KEY_PEDESTRIAN = "people";
	public static final String KEY_TRAFFIC_LIGHTS = "lights";
	public static final String KEY_TRAFFIC_SIGNS = "sign";

	private static final String CFG_NAME = "road_object.cfg";
	private static final String W_NAME = "road_object.weights";

	private static final double INPUT_IMAGE_SCALE = 1.0;//adjust size of input image frame
	private static final int BLOB_SIZE = 288;//adjust size of detection blob, the higher the more accurate but also slower. known available size 250/288/416/608/832
	private static final double MIN_DETECTION_CONFIDENCE = 0.3;
	private static final double NMS_THRESHOLD = 0.5;

	private static final String[] OBJECT_CLASSES = new String[]{
			"car",
			"truck",
			"bus",
			"motorcycle",
			"bicycle",
			"pedestrian",
			"light",
			"sign"
	};

	private Net net;

	private int frameHorizon;

	@Override
	public String getKey() {
		return KEY;
	}

	@Override
	long getMinUpdateInterval() {
		return 0;
	}

	@Override
	public void initialize() {
		super.initialize();
		handler.post(new Runnable() {
			@Override
			public void run() {
				readyForFrame = false;//shut this down
				InputStream is = null;
				FileOutputStream os = null;
				try {
					//thanks OpenCV, for somehow we can't just load the resource from android resource directory directly.
					// TODO no need for repeated loading. dump this somewhere else.
					File cfg = Utils.copyFileFromAssetToInternalStorage(ImageProcessingManager.getInstance().getContext(), CFG_NAME, "dnn");
					File weights = Utils.copyFileFromAssetToInternalStorage(ImageProcessingManager.getInstance().getContext(), W_NAME, "dnn");
					net = Dnn.readNetFromDarknet(cfg.getAbsolutePath(), weights.getAbsolutePath());
					Log.i(LOG_TAG, "neural net loaded");
					readyForFrame = true;
				} catch (Exception e) {
					Log.e(LOG_TAG, "Failed to load neural net", e);
				}


			}
		});
	}

	@Override
	Runnable process() {//TODO if slow, consider native method
		return new Runnable() {
			@Override
			public void run() {
				ArrayList<Vehicle> detectedVehicles = new ArrayList<>();
				ArrayList<TrafficLight> detectedTrafficLights = new ArrayList<>();
				ArrayList<TrafficSign> detectedTrafficSign = new ArrayList<>();
				frameHorizon = ImageProcessingManager.getInstance().getResultsMap().getInt(HorizonFinder.KEY, HorizonFinder.DEFAULT_HORIZON);
				int blobSize = BLOB_SIZE;//magic
				Size sz = new Size(blobSize, blobSize);
				float scale = 1.0F / 255.0F;//magic
				Mat inputBlob;
				if (INPUT_IMAGE_SCALE != 1.0) {
					Mat pMat = new Mat((int) (ImageProcessingManager.FRAME_HEIGHT * INPUT_IMAGE_SCALE), (int) (ImageProcessingManager.FRAME_WIDTH * INPUT_IMAGE_SCALE), CV_8UC1);
					Imgproc.resize(processingFrame, pMat, pMat.size(), 0, 0, Imgproc.INTER_LANCZOS4);
					inputBlob = Dnn.blobFromImage(pMat, scale, sz, new Scalar(0), false, false);
				} else {
					inputBlob = Dnn.blobFromImage(processingFrame, scale, sz, new Scalar(0), false, false);
				}
				net.setInput(inputBlob);
				List<Mat> result = new ArrayList<>();
				List<String> outBlobNames = getOutputLayerNames(net);
				net.forward(result, outBlobNames);
				List<Integer> classIds = new ArrayList<>();
				List<Float> confidence = new ArrayList<>();
				List<Rect> location = new ArrayList<>();
				for (Mat level : result) {
					for (int i = 0; i < level.rows(); ++ i) {
						/* Some explanation in case I forget what is what:
						for every item in entry:
						[0]: x center
						[1]: y center
						[2]: width
						[3]: height
						[4]: confidence level
						[5] and onward: probability for each of the object class , terminates at 4+object total count
					    */
						Mat row = level.row(i);
						Mat scores = row.colRange(5, level.cols());
						Core.MinMaxLocResult maxima = Core.minMaxLoc(scores);
						float conf = (float) maxima.maxVal;
						Point classIdPoint = maxima.maxLoc;
						if (conf > MIN_DETECTION_CONFIDENCE) {
							int centerX = (int) (row.get(0, 0)[0] * processingFrame.cols() * INPUT_IMAGE_SCALE);
							int centerY = (int) (row.get(0, 1)[0] * processingFrame.rows() * INPUT_IMAGE_SCALE);
							int width = (int) (row.get(0, 2)[0] * processingFrame.cols() * INPUT_IMAGE_SCALE);
							int height = (int) (row.get(0, 3)[0] * processingFrame.rows() * INPUT_IMAGE_SCALE);
							int left = Utils.clampValueToInt(centerX - width / 2.0, 0, ImageProcessingManager.FRAME_WIDTH);
							int top = Utils.clampValueToInt(centerY - height / 2.0, 0, ImageProcessingManager.FRAME_HEIGHT);
							classIds.add((int) classIdPoint.x);
							confidence.add(conf);
							location.add(new Rect(left, top, width, height));
						}
					}
				}
				if (! confidence.isEmpty()) {
					// Apply non-maxima suppression to filter redundant detections.
					MatOfFloat confidences = new MatOfFloat(Converters.vector_float_to_Mat(confidence));
					Rect[] boxesArray = location.toArray(new Rect[0]);
					MatOfRect boxes = new MatOfRect(boxesArray);
					MatOfInt indices = new MatOfInt();
					Dnn.NMSBoxes(boxes, confidences, (float) MIN_DETECTION_CONFIDENCE, (float) NMS_THRESHOLD, indices);
					for (int index : indices.toArray()) {
						int objClass = classIds.get(index);
						Rect box = boxesArray[index];
						Point tl = box.tl();
						Point br = box.br();
						switch (objClass) {
							case 0:
							case 1:
							case 2:
							case 3:
							case 4:
								Vehicle v = new Vehicle(new Rect2d(tl, br));
								if (filterSignals(detectedVehicles, v)) {
									detectedVehicles.add(v);
								}
								break;
							case 5:
								//nothing
								break;
							case 6:
								detectedTrafficLights.add(new TrafficLight(new Rect(tl, br), processingFrame));
								break;
							case 7:
								detectedTrafficSign.add(new SpeedSign(new Rect(tl, br), processingFrame));
								break;
							default:
								Log.w(LOG_TAG, "Detected object with unknown class " + objClass);
						}
					}
				}
				resultsMap.putMat(KEY_REF_IMAGE, processingFrame);
				resultsMap.putDetectedVehicleArray(KEY_VEHICLE, detectedVehicles.toArray(new Vehicle[0]));
				resultsMap.putDetectedLightsArray(KEY_TRAFFIC_LIGHTS, detectedTrafficLights.toArray(new TrafficLight[0]));
				resultsMap.putDetectedSignArray(KEY_TRAFFIC_SIGNS, detectedTrafficSign.toArray(new TrafficSign[0]));
			}
		};
	}

	private boolean filterSignals(ArrayList<Vehicle> vehicles, Vehicle target) {
		if (target.br().y > frameHorizon) {
			return true;
		}
		return false;
	}

	private static List<String> getOutputLayerNames(Net net) {
		List<String> names = new ArrayList<>();
		List<Integer> outLayers = net.getUnconnectedOutLayers().toList();
		List<String> layersNames = net.getLayerNames();
		for (Integer item : outLayers) {
			names.add(layersNames.get(item - 1));
		}
		return names;
	}
}
