/*
 * Copyright (c) 2019. Yip Chung Nin
 * Developed as part of project 18CS036 (DashDroid - Dash Cam with driver assistance) at City University of Hong Kong
 * All rights reserved
 */

package p18cs036.dashdroid.computervision.processors;

import android.util.Log;

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.tracking.Tracker;

import java.util.ArrayList;
import java.util.HashSet;

import p18cs036.dashdroid.Utils;
import p18cs036.dashdroid.computervision.ImageProcessingManager;
import p18cs036.dashdroid.computervision.model.Vehicle;

public class VehicleTracker extends ObjectTracker {
	private final String LOG_TAG = this.getClass().getName();
	public static final String KEY = "VEHICLE_TRACKER";

	private ArrayList<Vehicle> vehicles;

	@Override
	public String getKey() {
		return KEY;
	}

	@Override
	public void initialize() {
		super.initialize();
		handler.post(new Runnable() {
			@Override
			public void run() {
				readyForFrame = false;//shut this down
				vehicles = new ArrayList<>();
				readyForFrame = true;

			}
		});
	}

	@Override
	public boolean isReadyForFrame() {
		return super.isReadyForFrame() && vehicles.size() > 0;
	}

	@Override
	Runnable process() {
		return new Runnable() {
			@Override
			public void run() {
				if (vehicles.size() > 0) {
					Mat greyMat = preprocessImage(processingFrame);
					HashSet<Vehicle> lostVehicles = new HashSet<>();
					for (Vehicle v : vehicles) {
						if (! v.updatePosition(greyMat) || ! Utils.inRange(v.getCenterX(), 0, ImageProcessingManager.FRAME_WIDTH) || ! Utils.inRange(v.getCenterY(), 0, ImageProcessingManager.FRAME_HEIGHT)) {
							if (v.getTrackingFailureCount() > TRACKING_FAILURE_TOLERANCE) {
								Log.v(LOG_TAG, "Lost track on a detected vehicle");
								lostVehicles.add(v);
							}

						}
					}
					vehicles.removeAll(lostVehicles);
				}
				resultsMap.putDetectedVehicleArray(getKey(), vehicles.toArray(new Vehicle[0]));//TODO process results before returning
			}
		};
	}

	public void updateVehicleRef(final Mat refMat, final Vehicle[] objs) {
		handler.post(new Runnable() {
			@Override
			public void run() {
				for (Vehicle v : objs) {
					for (Vehicle v2 : vehicles) {
						if (v2.getRect().contains(new Point(v.getCenterX(), v.getCenterY()))) {
							v.setTracker(v2.getTracker());
							Tracker tracker = getDefaultTracker();
							tracker.init(refMat, v.getRect());
							v2.setTracker(tracker);
							break;
						}
					}
					if (v.getTracker() == null) {
						Tracker tracker = getDefaultTracker();
						tracker.init(refMat, v.getRect());
						v.setTracker(tracker);
						vehicles.add(v);
					}
				}
			}
		});
	}
}