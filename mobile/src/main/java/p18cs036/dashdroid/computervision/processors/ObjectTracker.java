/*
 * Copyright (c) 2019. Yip Chung Nin
 * Developed as part of project 18CS036 (DashDroid - Dash Cam with driver assistance) at City University of Hong Kong
 * All rights reserved
 */

package p18cs036.dashdroid.computervision.processors;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
import org.opencv.tracking.Tracker;
import org.opencv.tracking.TrackerBoosting;
import org.opencv.tracking.TrackerCSRT;
import org.opencv.tracking.TrackerKCF;
import org.opencv.tracking.TrackerMIL;
import org.opencv.tracking.TrackerMOSSE;
import org.opencv.tracking.TrackerMedianFlow;
import org.opencv.tracking.TrackerTLD;

public abstract class ObjectTracker extends ImageProcessor {
	private final String LOG_TAG = this.getClass().getName();
	public static final String KEY = "OBJ_TRACKER";

	protected int TRACKING_FAILURE_TOLERANCE = 3;

	private enum TrackerType {
		BOOSTING,
		MIL,
		KCF,
		TLD,
		MEDIANFLOW,
		//GOTURN,
		MOSSE,
		CSRT

	}

	private static final TrackerType trackerType = TrackerType.MOSSE;

	@Override
	long getMinUpdateInterval() {
		return 0;
	}


	protected Mat preprocessImage(Mat image) {
		Mat greyMat = new Mat();
		Mat gMat = new Mat();
		Imgproc.cvtColor(image, gMat, Imgproc.COLOR_RGB2GRAY);
		Imgproc.equalizeHist(gMat, greyMat);
		//return greyMat.submat( (int)(ImageProcessingManager.FRAME_HEIGHT * VEHICLE_PRESENT_THRESHOLD_UPPER), (int) (ImageProcessingManager.FRAME_HEIGHT * VEHICLE_PRESENT_THRESHOLD_LOWER), 0, greyMat.cols());TODO enable frame cropping to up the speed a bit
		return greyMat;
	}

	protected Tracker getDefaultTracker() {
		switch (trackerType) {
			case BOOSTING:
				return TrackerBoosting.create();
			case MIL:
				return TrackerMIL.create();
			case KCF:
				return TrackerKCF.create();
			case TLD:
				return TrackerTLD.create();
			case MEDIANFLOW:
				return TrackerMedianFlow.create();
			case MOSSE:
				return TrackerMOSSE.create();
			case CSRT:
				return TrackerCSRT.create();
			default:
				return TrackerBoosting.create();


		}
	}

}