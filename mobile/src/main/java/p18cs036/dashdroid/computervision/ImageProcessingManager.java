/*
 * Copyright (c) 2019. Yip Chung Nin
 * Developed as part of project 18CS036 (DashDroid - Dash Cam with driver assistance) at City University of Hong Kong
 * All rights reserved
 */

package p18cs036.dashdroid.computervision;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.preference.PreferenceManager;
import android.util.Log;

import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;

import java.util.ArrayDeque;
import java.util.ArrayList;

import p18cs036.dashdroid.ConcurrentKeyValueMap;
import p18cs036.dashdroid.DriveAssistant;
import p18cs036.dashdroid.R;
import p18cs036.dashdroid.computervision.processors.HorizonFinder;
import p18cs036.dashdroid.computervision.processors.IlluminationFinder;
import p18cs036.dashdroid.computervision.processors.ImageProcessor;
import p18cs036.dashdroid.computervision.processors.LaneStateDetector;
import p18cs036.dashdroid.computervision.processors.ObjectDetector;
import p18cs036.dashdroid.computervision.processors.VehicleTracker;

public class ImageProcessingManager {
	private final String LOG_TAG = this.getClass().getName();
	//instance
	private static ImageProcessingManager manager = new ImageProcessingManager();

	private static final int MAX_PROCESSING_FRAMERATE = 10;
	private static final int FRAME_BUFFER_SIZE = 3;//TODO is this good enough?
	public static final int FRAME_WIDTH = 1280;
	public static final int FRAME_HEIGHT = 720;
	public static final double POV_HEIGHT = 1.1;

	private Context context;

	private DriveAssistant driveAssistant;

	//own thread for running repeating stuff
	private HandlerThread thread;
	private Handler handler;


	private ArrayDeque<Mat> frameBuffer = new ArrayDeque<>();

	private ArrayList<ImageProcessor> cvProcessors;

	private ConcurrentKeyValueMap processResultsMap;


	private ImageProcessingManager() {

	}

	public static ImageProcessingManager getInstance() {
		return manager;
	}

	public Context getContext() {
		return context;
	}

	private void startThread() {
		thread = new HandlerThread("ImgProcessingThread");
		thread.start();
		handler = new Handler(thread.getLooper());
	}

	private void stopThread() {
		thread.quitSafely();
		try {
			thread.join();
			thread = null;
			handler = null;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void initialize(Context context) {
		this.context = context;
		driveAssistant = DriveAssistant.getInstance();
		OpenCVLoader.initDebug();
		Log.i(LOG_TAG, "Loaded OpenCV library");
		processResultsMap = new ConcurrentKeyValueMap();
		startThread();
		cvProcessors = new ArrayList<>();
		cvProcessors.add(new HorizonFinder());
		if (PreferenceManager.getDefaultSharedPreferences(context).getBoolean(context.getString(R.string.enable_illu_warn), true)) {
			cvProcessors.add(new IlluminationFinder());
		}
		if (PreferenceManager.getDefaultSharedPreferences(context).getBoolean(context.getString(R.string.enable_collision_warning), true) ||
				PreferenceManager.getDefaultSharedPreferences(context).getBoolean(context.getString(R.string.enable_safety_distance_warning), true) ||
				PreferenceManager.getDefaultSharedPreferences(context).getBoolean(context.getString(R.string.enable_light_warn), true) ||
				PreferenceManager.getDefaultSharedPreferences(context).getBoolean(context.getString(R.string.enable_speed_warn), true)) {
			cvProcessors.add(new ObjectDetector());
			if (PreferenceManager.getDefaultSharedPreferences(context).getBoolean(context.getString(R.string.enable_collision_warning), true) ||
					PreferenceManager.getDefaultSharedPreferences(context).getBoolean(context.getString(R.string.enable_safety_distance_warning), true)) {
				cvProcessors.add(new VehicleTracker());
			}
		}

		if (PreferenceManager.getDefaultSharedPreferences(context).getBoolean(context.getString(R.string.enable_lane_warn), true)) {
			cvProcessors.add(new LaneStateDetector());
		}
		for (ImageProcessor p : cvProcessors) {
			p.initialize();
		}
	}

	public void start() {
		handler.post(new Runnable() {
			@Override
			public void run() {
				while (frameBuffer.size() > FRAME_BUFFER_SIZE) {//dump old frames - they are probably useless now.
					Log.d(LOG_TAG, "Skipping frame" + frameBuffer.remove());
				}
				if (! frameBuffer.isEmpty()) {
					Mat frame = frameBuffer.remove();
					Log.v(LOG_TAG, "handing frame " + frame + "for process");
					for (ImageProcessor p : cvProcessors) {
						if (p.isRequestingUpdate()) {
							if (p.isReadyForFrame()) {
								p.processFrame(frame);
							} else {
								Log.d(LOG_TAG, "Processor " + p.getKey() + " is busy, skipping");
							}
						}
					}
				} else {
					Log.d(LOG_TAG, "Frame buffer is empty, no frame to process");
				}
				handler.postDelayed(this, 1000 / MAX_PROCESSING_FRAMERATE);
			}
		});
	}

	public void queueFrameForProcessing(Mat image) {
		frameBuffer.add(image);
		Log.v(LOG_TAG, "Add frame " + image + " to queue.");
	}

	public ConcurrentKeyValueMap getResultsMap() {
		return processResultsMap;
	}

	public void onResultsUpdated(final String key) {
		if (handler != null) {
			handler.post(new Runnable() {
				@Override
				public void run() {
					switch (key) {
						case ObjectDetector.KEY:
							for (ImageProcessor p : cvProcessors) {
								Mat ref = getResultsMap().getMat(ObjectDetector.KEY_REF_IMAGE);
								if (p instanceof VehicleTracker) {
									VehicleTracker oj = (VehicleTracker) p;
									Log.d(LOG_TAG, "Updating vehicle info for tracking");
									oj.updateVehicleRef(ref, getResultsMap().getDetectedVehicleArray(ObjectDetector.KEY_VEHICLE));
								}
							}
							driveAssistant.onNewTrafficLightData();
							driveAssistant.onNewRoadSignData();
							break;
						case VehicleTracker.KEY:
							driveAssistant.onNewVehicleData();
							break;
						case LaneStateDetector.KEY:
							driveAssistant.onLaneStatusUpdated();
							break;
						case IlluminationFinder.KEY:
							driveAssistant.onIlluminationsUpdated();
							break;
						default:
							Log.w(LOG_TAG, "Unknown processor instance: " + key);

					}
				}
			});
		}

	}


	public void stop() {
		stopThread();
		for (ImageProcessor p : cvProcessors) {
			p.stop();
		}
	}


}
