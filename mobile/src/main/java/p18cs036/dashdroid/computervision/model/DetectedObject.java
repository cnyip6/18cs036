/*
 * Copyright (c) 2019. Yip Chung Nin
 * Developed as part of project 18CS036 (DashDroid - Dash Cam with driver assistance) at City University of Hong Kong
 * All rights reserved
 */

package p18cs036.dashdroid.computervision.model;

import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Rect2d;

public class DetectedObject {

	private int centerX;
	private int centerY;
	private Rect2d rect;

	public DetectedObject(Rect image) {
		this(new Rect2d(image.tl(), image.br()));
	}

	public DetectedObject(Rect2d image) {
		setRect(image);

	}

	public int getCenterX() {
		return centerX;
	}

	public int getCenterY() {
		return centerY;
	}

	public int getWidth() {
		return (int) rect.width;
	}

	public int getHeight() {
		return (int) rect.height;
	}

	public Point tl() {
		return rect.tl();
	}

	public Point br() {
		return rect.br();
	}

	public Rect2d getRect() {
		return rect;
	}

	public void setRect(Rect2d image) {
		rect = image;
		centerX = (int) ((image.tl().x + image.br().x) * 0.5);
		centerY = (int) ((image.tl().y + image.br().y) * 0.5);
	}

	public void setRect(Rect image) {
		setRect(new Rect2d(image.tl(), image.br()));
	}

	public boolean contains(DetectedObject object) {
		return rect.contains(object.rect.tl()) && rect.contains(object.rect.br());
	}
}
