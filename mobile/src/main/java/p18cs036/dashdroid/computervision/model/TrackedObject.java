/*
 * Copyright (c) 2019. Yip Chung Nin
 * Developed as part of project 18CS036 (DashDroid - Dash Cam with driver assistance) at City University of Hong Kong
 * All rights reserved
 */

package p18cs036.dashdroid.computervision.model;

import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Rect2d;
import org.opencv.tracking.Tracker;

import java.util.ArrayList;

import p18cs036.dashdroid.ConcurrentKeyValueMap;
import p18cs036.dashdroid.Utils;
import p18cs036.dashdroid.computervision.ImageProcessingManager;
import p18cs036.dashdroid.computervision.processors.HorizonFinder;
import p18cs036.dashdroid.dashcam.DashCam;
import p18cs036.dashdroid.dashcam.DataRecorder;

import static p18cs036.dashdroid.computervision.ImageProcessingManager.POV_HEIGHT;

public class TrackedObject extends DetectedObject {
	private Tracker tracker = null;

	private static final int RANGE_RATE_SAMPLE_SIZE = 20;

	private boolean trackingFailed = false;
	private int trackingFailureCount = 0;


	private Double lateralDisplacement = null;
	private Double forwardDisplacement = null;

	private ArrayList<Double> ranges = new ArrayList<>();
	private ArrayList<Double> avgRanges = new ArrayList<>();

	public TrackedObject(Rect location) {
		super(location);
	}

	public TrackedObject(Rect2d location) {
		super(location);
	}

	@Override
	public void setRect(Rect image) {
		super.setRect(image);
		updateRange();
	}

	public void setTracker(Tracker tracker) {
		this.tracker = tracker;
	}

	public Tracker getTracker() {
		return tracker;
	}

	public void onTrackingFailed() {
		trackingFailed = true;
		trackingFailureCount++;
	}

	public void onTracked() {
		trackingFailed = false;
		trackingFailureCount = 0;
	}

	public boolean isTrackingFailed() {
		return trackingFailed;
	}

	public int getTrackingFailureCount() {
		return trackingFailureCount;
	}

	public boolean updatePosition(Mat frame) {
		Rect2d loc = new Rect2d();
		if (getTracker().update(frame, loc)) {
			setRect(loc);
			onTracked();
			return true;
		} else {
			onTrackingFailed();
			return false;
		}
	}

	public Double getLateralDisplacement() {
		return lateralDisplacement;
	}

	public Double getForwardDisplacement() {
		return forwardDisplacement;
	}

	public void updateRange() {
		//TODO Verify the validity of algorithm
		ConcurrentKeyValueMap map = ImageProcessingManager.getInstance().getResultsMap();
		double focalLengthY = map.getDouble(DashCam.DATA_KEY_CAM_FOCAL_LENGTH_Y);
		double focalLengthX = map.getDouble(DashCam.DATA_KEY_CAM_FOCAL_LENGTH_X);
		double copY = map.getDouble(DashCam.DATA_KEY_CAM_CENTER_OF_VIEW_Y);
		double copX = map.getDouble(DashCam.DATA_KEY_CAM_CENTER_OF_VIEW_X);
		double pitch = Math.toRadians(map.getDouble(HorizonFinder.KEY_PITCH, map.getDouble(DataRecorder.DATA_KEY_PITCH, 0.0)));
		//lateral displacement using Ponsa's algorithm
		lateralDisplacement = (POV_HEIGHT * focalLengthY * (copX - getCenterX())) / (focalLengthX * (copY - br().y) * Math.cos(pitch) + focalLengthX * focalLengthY * Math.sin(pitch));
		//forward displacement using Ponsa's algorithm
		forwardDisplacement = (POV_HEIGHT * ((copY - br().y) * Math.sin(pitch) - focalLengthY * Math.cos(pitch))) / ((copY - br().y) * Math.cos(pitch) + focalLengthY * Math.sin(pitch));
		//basic math
		double range = Math.sqrt(lateralDisplacement * lateralDisplacement + forwardDisplacement * forwardDisplacement);
		ranges.add(range);
		avgRanges.add(Utils.lowPassFilter(ranges, 100, 30));
	}

	public Double getRange() {
		if (ranges.size() > 0) {
			return ranges.get(ranges.size() - 1);
		}
		return null;
	}

	public Double getRangeRate() {
		if (avgRanges.size() >= 2) {
			return Utils.getLastElementOfList(avgRanges) - avgRanges.get(avgRanges.size() - 2);
		} else {
			return 0.0;
		}
	}

	@Override
	public String toString() {
		return super.toString() + " Distance= " + getRange();
	}
}

