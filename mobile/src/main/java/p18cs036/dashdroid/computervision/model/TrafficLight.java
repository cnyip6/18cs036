/*
 * Copyright (c) 2019. Yip Chung Nin
 * Developed as part of project 18CS036 (DashDroid - Dash Cam with driver assistance) at City University of Hong Kong
 * All rights reserved
 */

package p18cs036.dashdroid.computervision.model;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Rect;

public class TrafficLight extends TrackedObject {
	private final double LIGHT_ON_THRESHOLD = 1.7;
	private Mat image;
	private double averageBrightness;

	public TrafficLight(Rect location, Mat frame) {
		super(location);
		image = frame.submat(location);
		averageBrightness = Core.mean(image).val[0];
	}

	public boolean isRed() {
		//assuming red light is always on top
		return Core.mean(image.submat(0, (int) (image.rows() * 0.3), 0, image.cols())).val[0] / averageBrightness > LIGHT_ON_THRESHOLD;
	}

	public boolean isGreen() {
		//assuming green light always at bottom
		return Core.mean(image.submat((int) (image.rows() * 0.6), image.rows(), 0, image.cols())).val[0] / averageBrightness > LIGHT_ON_THRESHOLD;
	}
}
