/*
 * Copyright (c) 2019. Yip Chung Nin
 * Developed as part of project 18CS036 (DashDroid - Dash Cam with driver assistance) at City University of Hong Kong
 * All rights reserved
 */

package p18cs036.dashdroid.computervision.model;

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;

public class SpeedSign extends TrafficSign {
	private static final double EXTRACTION_WIDTH = 0.7;
	private static final double EXTRACTION_HEIGHT = 0.48;

	public SpeedSign(Rect location, Mat frame) {
		super(location, frame);
		//assuming that the original rect completely envelops the sign, need to remove the border to improve the text recongnition.
		//for simplicity, using hardcoded position for now
		//TODO if have time: detection the actual postion of text and crop the image
		image = image.submat(new Rect(
				new Point(image.cols() / 2.0 - image.cols() * EXTRACTION_WIDTH / 2, image.rows() / 2.0 - image.rows() * EXTRACTION_HEIGHT / 2),
				new Point(image.cols() / 2.0 + image.cols() * EXTRACTION_WIDTH / 2, image.rows() / 2.0 + image.rows() * EXTRACTION_HEIGHT / 2)
		));
	}
}
