/*
 * Copyright (c) 2019. Yip Chung Nin
 * Developed as part of project 18CS036 (DashDroid - Dash Cam with driver assistance) at City University of Hong Kong
 * All rights reserved
 */

package p18cs036.dashdroid.computervision.model;

import org.opencv.core.Rect;
import org.opencv.core.Rect2d;

import java.util.Collection;
import java.util.HashSet;

import p18cs036.dashdroid.ConcurrentKeyValueMap;
import p18cs036.dashdroid.computervision.ImageProcessingManager;
import p18cs036.dashdroid.dashcam.DashCam;
import p18cs036.dashdroid.dashcam.DataRecorder;

public class Vehicle extends TrackedObject {

	private static final int RANGE_RATE_SAMPLE_SIZE = 3;

	private Double vehicleHeight = null;

	public Vehicle(Rect image) {
		super(image);
	}

	public Vehicle(Rect2d image) {
		super(image);
	}

	@Override
	public void setRect(Rect image) {
		super.setRect(image);
		updateEstimatedHeight();
	}

	public boolean overlapped(Collection<Vehicle> vehicles) {
		HashSet<Vehicle> overlapped = new HashSet<>();
		for (Vehicle v : vehicles) {
			if (v.contains(this)) {
				return true;
			}
			if (contains(v)) {
				overlapped.add(v);
			}
		}
		vehicles.removeAll(overlapped);
		return false;
	}

	public Double getEstimatedHeight() {
		return vehicleHeight;
	}

	public void updateEstimatedHeight() {
		ConcurrentKeyValueMap map = ImageProcessingManager.getInstance().getResultsMap();
		//setch all variables
		double focalLengthY = map.getDouble(DashCam.DATA_KEY_CAM_FOCAL_LENGTH_Y);
		double focalLengthX = map.getDouble(DashCam.DATA_KEY_CAM_FOCAL_LENGTH_X);
		double copY = map.getDouble(DashCam.DATA_KEY_CAM_CENTER_OF_VIEW_Y);
		double pitch = Math.toRadians(map.getDouble(DataRecorder.DATA_KEY_PITCH));//TODO possible to sync pitch and image?
		//treating the area as a square with same Height and width
		double newHeight = focalLengthY * getHeight() / (focalLengthX * (copY - getHeight()) * Math.cos(pitch) + focalLengthX * focalLengthY * Math.sin(pitch));
		if (vehicleHeight != null) {
			vehicleHeight = (vehicleHeight + newHeight) / 2;
		} else {
			vehicleHeight = newHeight;
		}
	}
}
