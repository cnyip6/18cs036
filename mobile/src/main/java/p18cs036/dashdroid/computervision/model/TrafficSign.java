/*
 * Copyright (c) 2019. Yip Chung Nin
 * Developed as part of project 18CS036 (DashDroid - Dash Cam with driver assistance) at City University of Hong Kong
 * All rights reserved
 */

package p18cs036.dashdroid.computervision.model;

import org.opencv.core.Mat;
import org.opencv.core.Rect;

public class TrafficSign extends DetectedObject {
	protected Mat image;

	public TrafficSign(Rect location, Mat frame) {
		super(location);
		image = frame.submat(location);
	}

	public Mat getImage() {
		return image;
	}

	public String getContent() {
		//TODO perform OCR and get content
		return "";
	}
}