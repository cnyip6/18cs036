/*
 * Copyright (c) 2019. Yip Chung Nin
 * Developed as part of project 18CS036 (DashDroid - Dash Cam with driver assistance) at City University of Hong Kong
 * All rights reserved
 */

package p18cs036.dashdroid.dashcam;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Surface;
import android.view.WindowManager;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import p18cs036.dashdroid.ConcurrentKeyValueMap;
import p18cs036.dashdroid.DriveAssistant;
import p18cs036.dashdroid.R;
import p18cs036.dashdroid.Utils;
import p18cs036.dashdroid.computervision.ImageProcessingManager;

public class DataRecorder {

	private final String LOG_TAG = this.getClass().getName();

	private static final int DATA_UPDATE_RATE = 10;//per second

	public static final String DATA_KEY_SPEED = "SPEED";
	public static final String DATA_KEY_HEADING = "HEADING";
	public static final String DATA_KEY_LATITUDE = "LAT";
	public static final String DATA_KEY_LONGITUDE = "LONG";
	public static final String DATA_KEY_PITCH = "PITCH";
	public static final String DATA_KEY_ROLL = "ROLL";

	private Context context;

	//own thread to run
	private HandlerThread thread;
	private Handler handler;

	private SensorManager sensorManager;
	private LocationManager locationManager;

	//sensors
	private Sensor acclerometer;
	private Sensor linearAcclerometer;
	private Sensor magnetometer;
	private Sensor gyroscope;

	//sensor readings buffers
	private final float[] accelerometerReadings = new float[3];
	private final float[] linearAcceleration = new float[3];
	private final float[] magnetometerReadings = new float[3];
	private final float[] gyroscopeReadings = new float[3];

	//Results
	private volatile double currentSpeed;
	private volatile double currentHeading;
	private volatile double currentLatitude;
	private volatile double currentLongitude;
	private volatile double currentPitch;
	private volatile double currentRoll;

	private StringBuilder records;

	public DataRecorder(Context context) {
		this.context = context;
	}

	private void startThread() {
		thread = new HandlerThread("DataRecorderBackground");
		thread.start();
		handler = new Handler(thread.getLooper());

	}

	private void stopThread() {
		thread.quitSafely();
		try {
			thread.join();
			thread = null;
			handler = null;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}


	public void initialize() {
		sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
		locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		linearAcclerometer = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
		acclerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		magnetometer = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
		gyroscope = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
		records = new StringBuilder();
		startThread();
	}

	public void startRecording() {
		if (linearAcclerometer != null) {
			sensorManager.registerListener(sensorListener, linearAcclerometer, 1000 / DATA_UPDATE_RATE * 1000, 0, handler);
		} else {
			Log.w(LOG_TAG, "No linear accleration");
		}
		if (acclerometer != null) {
			sensorManager.registerListener(sensorListener, acclerometer, 1000 / DATA_UPDATE_RATE * 1000, 0, handler);
		} else {
			Log.w(LOG_TAG, "acclerometer not found");
		}
		if (magnetometer != null) {
			sensorManager.registerListener(sensorListener, magnetometer, 1000 / DATA_UPDATE_RATE * 1000, 0, handler);
		} else {
			Log.w(LOG_TAG, "magnetometer not found");
		}
		if (gyroscope != null) {
			sensorManager.registerListener(sensorListener, gyroscope, 1000 / DATA_UPDATE_RATE * 1000, 0, handler);
		} else {
			Log.w(LOG_TAG, "gyroscope not found");
		}
		if (! locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			//Utils.getAlertDialog(context, context.getString(android.R.string.dialog_alert_title), context.getString(R.string.GPS_disabled)).show();
			//TODO lead user to setting or crash app
			Log.w(LOG_TAG, "GPS disabled");
			DriveAssistant.getInstance().warnNoGPS();
		}
		try {
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener, thread.getLooper());
		} catch (SecurityException e) {
			Log.e(LOG_TAG, e.getMessage());
			e.printStackTrace();
		}
		handler.post(new Runnable() {
			@Override
			public void run() {
				writeRecord();
				handler.postDelayed(this, 1000 / DATA_UPDATE_RATE);
			}
		});

	}

	private SensorEventListener sensorListener = new SensorEventListener() {
		@Override
		public void onSensorChanged(SensorEvent sensorEvent) {
			switch (sensorEvent.sensor.getType()) {
				case Sensor.TYPE_ACCELEROMETER:
					System.arraycopy(sensorEvent.values, 0, accelerometerReadings, 0, accelerometerReadings.length);
					break;
				case Sensor.TYPE_LINEAR_ACCELERATION:
					System.arraycopy(sensorEvent.values, 0, linearAcceleration, 0, linearAcceleration.length);
					break;
				case Sensor.TYPE_MAGNETIC_FIELD:
					System.arraycopy(sensorEvent.values, 0, magnetometerReadings, 0, magnetometerReadings.length);
					break;
				case Sensor.TYPE_GYROSCOPE:
					System.arraycopy(sensorEvent.values, 0, gyroscopeReadings, 0, gyroscopeReadings.length);

			}

		}

		@Override
		public void onAccuracyChanged(Sensor sensor, int i) {

		}
	};

	private LocationListener locationListener = new LocationListener() {
		@Override
		public void onLocationChanged(Location location) {
			currentLatitude = location.getLatitude();
			currentLongitude = location.getLongitude();
			currentSpeed = location.getSpeed();
		}

		@Override
		public void onStatusChanged(String s, int i, Bundle bundle) {

		}

		@Override
		public void onProviderEnabled(String s) {

		}

		@Override
		public void onProviderDisabled(String s) {

		}
	};

	private void writeRecord() {
//derive positional data
		final float[] rotationMatrix = new float[9];
		SensorManager.getRotationMatrix(rotationMatrix, null, accelerometerReadings, magnetometerReadings);
		float[] orientationAngles = convertCoordinateMapping(rotationMatrix);
		currentHeading = orientationAngles[0];
		currentPitch = orientationAngles[1];
		currentRoll = orientationAngles[2];
		List<String> record = new ArrayList<>();
		record.add(String.valueOf(Utils.getReadableTimestamp()));
		record.add(String.valueOf(currentHeading));
		record.add(String.valueOf(currentPitch));
		record.add(String.valueOf(currentRoll));
		record.add(Location.convert(currentLatitude, Location.FORMAT_SECONDS));
		record.add(Location.convert(currentLongitude, Location.FORMAT_SECONDS));
		record.add(String.valueOf(currentSpeed));
		String recordString = Utils.jointStringListToString(record, ",");
		Log.v(LOG_TAG, recordString);
		records.append(recordString).append(System.getProperty("line.separator"));
		//also write to result map
		ConcurrentKeyValueMap resultMap = ImageProcessingManager.getInstance().getResultsMap();
		if (PreferenceManager.getDefaultSharedPreferences(context).getBoolean(context.getString(R.string.enable_drive_assist), true)) {
			resultMap.putDouble(DATA_KEY_HEADING, currentHeading);
			resultMap.putDouble(DATA_KEY_LATITUDE, currentLatitude);
			resultMap.putDouble(DATA_KEY_LONGITUDE, currentLongitude);
			resultMap.putDouble(DATA_KEY_PITCH, Utils.invertSign(currentPitch));//sign is inverted to fit general definition of pitch
			resultMap.putDouble(DATA_KEY_ROLL, currentRoll);
			resultMap.putDouble(DATA_KEY_SPEED, currentSpeed);
		}
	}

	private float[] convertCoordinateMapping(float[] rotationalMatrix) {
		float[] newRotationalMatrix = new float[9];
		if (SensorManager.remapCoordinateSystem(rotationalMatrix, SensorManager.AXIS_X, SensorManager.AXIS_Z, newRotationalMatrix)) {//compensate for upright position
			float[] remappedOrientation = new float[3];
			float[] remappedOrientationDeg = new float[3];
			SensorManager.getOrientation(newRotationalMatrix, remappedOrientation);
			for (int i = 0; i < remappedOrientation.length; i++) {
				remappedOrientationDeg[i] = (float) Math.toDegrees(remappedOrientation[i]);
			}
			int rotation = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getRotation();
			//compensate for screen rotation
			switch (rotation) {
				case Surface.ROTATION_0:
					//it's okay
					break;
				case Surface.ROTATION_90:
					remappedOrientationDeg[2] += 90;
					break;
				case Surface.ROTATION_180:
					remappedOrientationDeg[2] += 180;
					break;
				case Surface.ROTATION_270:
					remappedOrientationDeg[2] += 270;
					break;
			}
			return remappedOrientationDeg;
		}
		return null;//whoops
	}


	public void stopRecording() {
		sensorManager.unregisterListener(sensorListener);
		locationManager.removeUpdates(locationListener);
		stopThread();
		writeDataFile();
	}

	public void writeDataFile() {
		//TODO match data and video file name
		try {
			Utils.writeTextFileToExternalStorage(records.toString(), null, getOutputFile().getCanonicalPath());
		} catch (IOException e) {
			Log.e(LOG_TAG, e.getLocalizedMessage(), e);
		}
	}

	private File getOutputFile() {
		//TODO place this in settings?
		File dir = new File(Environment.getExternalStorageDirectory().getPath() + "/" + context.getString(R.string.app_name));
		if (! dir.exists()) {
			dir.mkdir();
		}
		return new File(dir.getAbsolutePath() + "/" + Utils.getReadableTimestamp() + ".csv");
	}

}
