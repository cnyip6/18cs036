/*
 * Copyright (c) 2019. Yip Chung Nin
 * Developed as part of project 18CS036 (DashDroid - Dash Cam with driver assistance) at City University of Hong Kong
 * All rights reserved
 */

package p18cs036.dashdroid.dashcam;

import android.content.Context;
import android.graphics.ImageFormat;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.media.CamcorderProfile;
import android.media.Image;
import android.media.ImageReader;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.util.Log;
import android.util.SizeF;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.WindowManager;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import p18cs036.dashdroid.ConcurrentKeyValueMap;
import p18cs036.dashdroid.R;
import p18cs036.dashdroid.Utils;
import p18cs036.dashdroid.computervision.ImageProcessingManager;

public class DashCam {
	private final String LOG_TAG = this.getClass().getName();

	private Context context;

	//own thread for running
	private HandlerThread thread;
	private Handler handler;

	//camera controlling components
	private CameraManager cameraManager;//using new API
	private CameraDevice camera;

	//
	private MediaRecorder mediaRecorder;
	private ImageReader frameReader;

	public static final String DATA_KEY_CAM_EXPOSURE = "CAM_EXPOSURE";
	public static final String DATA_KEY_CAM_FOCAL_LENGTH_X = "F_LENGTH_X";
	public static final String DATA_KEY_CAM_FOCAL_LENGTH_Y = "F_LENGTH_Y";
	public static final String DATA_KEY_CAM_CENTER_OF_VIEW_X = "COP_X";
	public static final String DATA_KEY_CAM_CENTER_OF_VIEW_Y = "COP_Y";

	public DashCam(Context context) {
		this.context = context;
	}

	private static final SparseIntArray ORIENTATIONS = new SparseIntArray();

	static {
		ORIENTATIONS.append(Surface.ROTATION_0, 90);
		ORIENTATIONS.append(Surface.ROTATION_90, 0);
		ORIENTATIONS.append(Surface.ROTATION_180, 270);
		ORIENTATIONS.append(Surface.ROTATION_270, 180);
	}

	private void startThread() {
		thread = new HandlerThread("DashCamThread");
		thread.start();
		handler = new Handler(thread.getLooper());
	}

	private void stopThread() {
		thread.quitSafely();
		try {
			thread.join();
			thread = null;
			handler = null;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void initialize() {
		String cameraId = null;
		startThread();
		cameraManager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);
		try {
			for (String id : cameraManager.getCameraIdList()) {
				CameraCharacteristics characteristics = cameraManager.getCameraCharacteristics(id);
				if (CameraCharacteristics.LENS_FACING_BACK == characteristics.get(CameraCharacteristics.LENS_FACING)) {
					cameraId = id;
					if (PreferenceManager.getDefaultSharedPreferences(context).getBoolean(context.getString(R.string.enable_drive_assist), true)) {
						ConcurrentKeyValueMap keyValueMap = ImageProcessingManager.getInstance().getResultsMap();
						if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && characteristics.getKeys().contains(CameraCharacteristics.LENS_INTRINSIC_CALIBRATION)) {
							float[] intrinsics = cameraManager.getCameraCharacteristics(id).get(CameraCharacteristics.LENS_INTRINSIC_CALIBRATION);
							keyValueMap.putDouble(DATA_KEY_CAM_FOCAL_LENGTH_X, intrinsics[0]);
							keyValueMap.putDouble(DATA_KEY_CAM_CENTER_OF_VIEW_Y, intrinsics[1]);
							keyValueMap.putDouble(DATA_KEY_CAM_CENTER_OF_VIEW_X, intrinsics[2]);
							keyValueMap.putDouble(DATA_KEY_CAM_CENTER_OF_VIEW_Y, intrinsics[3]);
						} else {
							Log.w(LOG_TAG, "Unable to obtain camera calibration data, assuming perfect lens");
							//assume no lens distortion:
							//1. focal length x is same as focal length y
							//2. center fo projection same as center of image
							double focalLength = characteristics.get(CameraCharacteristics.LENS_INFO_AVAILABLE_FOCAL_LENGTHS)[0];
							SizeF sensorSize = characteristics.get(CameraCharacteristics.SENSOR_INFO_PHYSICAL_SIZE);
							keyValueMap.putDouble(DATA_KEY_CAM_FOCAL_LENGTH_X, focalLength / sensorSize.getWidth() * ImageProcessingManager.FRAME_WIDTH);//converting unit from mm to pixel
							keyValueMap.putDouble(DATA_KEY_CAM_FOCAL_LENGTH_Y, focalLength / sensorSize.getHeight() * ImageProcessingManager.FRAME_HEIGHT);
							Log.d(LOG_TAG, "Focal length = " + keyValueMap.getDouble(DATA_KEY_CAM_FOCAL_LENGTH_Y));
							keyValueMap.putDouble(DATA_KEY_CAM_CENTER_OF_VIEW_X, ImageProcessingManager.FRAME_WIDTH / 2);
							keyValueMap.putDouble(DATA_KEY_CAM_CENTER_OF_VIEW_Y, ImageProcessingManager.FRAME_HEIGHT / 2);
						}
					}
					break;
				}
			}
			cameraManager.openCamera(cameraId, new CameraDevice.StateCallback() {
				@Override
				public void onOpened(@NonNull CameraDevice cameraDevice) {
					camera = cameraDevice;
				}

				@Override
				public void onDisconnected(@NonNull CameraDevice cameraDevice) {

				}

				@Override
				public void onError(@NonNull CameraDevice cameraDevice, int i) {

				}
			}, handler);
		} catch (CameraAccessException | SecurityException e) {
			Log.d(LOG_TAG, "Error while initializing camera", e);
		}

	}

	public void startRecording() {
		final List<Surface> surfaceList = new ArrayList<>();
		try {
			mediaRecorder = getMediaRecorder();
			frameReader = getFrameReader();
			surfaceList.add(mediaRecorder.getSurface());
			if (PreferenceManager.getDefaultSharedPreferences(context).getBoolean(context.getString(R.string.enable_drive_assist), true)) {
				surfaceList.add(frameReader.getSurface());
			}
			camera.createCaptureSession(
					surfaceList,
					new CameraCaptureSession.StateCallback() {
						@Override
						public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
							try {
								CaptureRequest.Builder builder = camera.createCaptureRequest(CameraDevice.TEMPLATE_RECORD);
								for (Surface s : surfaceList) {
									builder.addTarget(s);
								}
								builder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
								builder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
								builder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON);
								builder.set(CaptureRequest.JPEG_ORIENTATION, getImageOrientation());
								CaptureRequest request = builder.build();
								cameraCaptureSession.setRepeatingRequest(request, new CameraCaptureSession.CaptureCallback() {
									@Override
									public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
										super.onCaptureCompleted(session, request, result);
										if (ImageProcessingManager.getInstance().getResultsMap() != null) {
											ImageProcessingManager.getInstance().getResultsMap().putInt(DATA_KEY_CAM_EXPOSURE, result.get(CaptureResult.SENSOR_SENSITIVITY));
										}
									}
								}, handler);
								mediaRecorder.start();
							} catch (CameraAccessException e) {
								Log.e(LOG_TAG, e.getMessage(), e);
								e.printStackTrace();

							}
						}

						@Override
						public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
							Log.e(LOG_TAG, "Fail to configure camera");
						}
					}, handler);
		} catch (Exception e) {
			Log.e(LOG_TAG, e.getMessage(), e);
			e.printStackTrace();
		}
	}

	private MediaRecorder getMediaRecorder() throws IOException {
		MediaRecorder recorder = new MediaRecorder();
		recorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
		recorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);
		recorder.setOutputFile(getOutputFile().getAbsolutePath());
		recorder.setProfile(getCamcorderProfile());
		int deviceRotation = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getRotation();
		recorder.setOrientationHint(ORIENTATIONS.get(deviceRotation));

		recorder.prepare();
		return recorder;
	}

	private CamcorderProfile getCamcorderProfile() {
		CamcorderProfile camcorderProfile = null;
		String[] resolutions = context.getResources().getStringArray(R.array.visdeo_settings_resolutions_values);//TODO find way to ensure mapping is correct
		String set_resolution = PreferenceManager.getDefaultSharedPreferences(context).getString(context.getString(R.string.video_resolution), null);
		Log.d(LOG_TAG, "Set resolution = " + set_resolution);
		Log.d(LOG_TAG, resolutions[0]);
		if (resolutions[0].equals(set_resolution)) {
			camcorderProfile = CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH);
		} else if (resolutions[1].equals(set_resolution)) {
			if (CamcorderProfile.hasProfile(CameraMetadata.LENS_FACING_BACK, CamcorderProfile.QUALITY_1080P)) {
				camcorderProfile = CamcorderProfile.get(CamcorderProfile.QUALITY_1080P);
			}
		} else if (resolutions[2].equals(set_resolution)) {
			if (CamcorderProfile.hasProfile(CamcorderProfile.QUALITY_720P)) {
				camcorderProfile = CamcorderProfile.get(CamcorderProfile.QUALITY_720P);
			}
		}
		if (camcorderProfile == null) {//catch all
			camcorderProfile = CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH);
		}
		//TODO recording without sound?
		return camcorderProfile;
	}

	private ImageReader getFrameReader() {
		ImageReader reader = ImageReader.newInstance(ImageProcessingManager.FRAME_WIDTH, ImageProcessingManager.FRAME_HEIGHT, ImageFormat.YUV_420_888, 5);
		reader.setOnImageAvailableListener(new ImageReader.OnImageAvailableListener() {
			@Override
			public void onImageAvailable(ImageReader imageReader) {
				Image image = imageReader.acquireNextImage();
				Log.v(LOG_TAG, "frame received from camera" + image);
				ImageProcessingManager.getInstance().queueFrameForProcessing(Utils.imageToRGBMat(image));
				image.close();
			}
		}, handler);
		return reader;
	}

	private int getImageOrientation() {
		final int rotation = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getRotation();
		return ORIENTATIONS.get(rotation);
	}

	//todo move this
	private File getOutputFile() {
		//TODO place this in settings?
		File dir = new File(Environment.getExternalStorageDirectory().getPath() + "/" + context.getString(R.string.app_name));
		if (! dir.exists()) {
			dir.mkdir();
		}
		return new File(dir.getAbsolutePath() + "/" + Utils.getReadableTimestamp() + ".mp4");
	}

	public void stopRecording() {
		mediaRecorder.stop();
		mediaRecorder.reset();
		mediaRecorder.release();
		camera.close();
		stopThread();
	}


}
