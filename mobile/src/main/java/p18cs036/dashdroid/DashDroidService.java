/*
 * Copyright (c) 2019. Yip Chung Nin
 * Developed as part of project 18CS036 (DashDroid - Dash Cam with driver assistance) at City University of Hong Kong
 * All rights reserved
 */

package p18cs036.dashdroid;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.ImageReader;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import p18cs036.dashdroid.computervision.ImageProcessingManager;
import p18cs036.dashdroid.dashcam.DashCam;
import p18cs036.dashdroid.dashcam.DataRecorder;

public class DashDroidService extends Service {
	private final String LOG_TAG = this.getClass().getName();

	private final static int SERVICE_NOTIFICATION_ID = 1;

	static boolean isRunning = false;

	private boolean enableDriveAssist;

	//own thread for running, just to be safe
	protected HandlerThread thread;
	protected Handler handler;


	private DashCam dashCam = new DashCam(this);
	private DataRecorder dataRecorder = new DataRecorder(this);
	private ImageProcessingManager imageProcessor = ImageProcessingManager.getInstance();
	private DriveAssistant driveAssistant = DriveAssistant.getInstance();

	private ImageReader frameReader;

	public DashDroidService() {
	}

	protected void startThread() {
		thread = new HandlerThread("DashCamThread");
		thread.start();
		handler = new Handler(thread.getLooper());
	}

	protected void stopThread() {
		thread.quitSafely();
		try {
			thread.join();
			thread = null;
			handler = null;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onCreate() {
		super.onCreate();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
			NotificationChannel channel = new NotificationChannel(
					getString(R.string.notify_channel_id),
					getString(R.string.notify_channel_id),
					NotificationManager.IMPORTANCE_DEFAULT
			);
			channel.setDescription(getString(R.string.notify_channel_description));
			notificationManager.createNotificationChannel(channel);
		}
		Notification notification = new NotificationCompat.Builder(this, getString(R.string.notify_channel_id))
				.setContentTitle(getText(R.string.app_name))
				.setContentText(getText(R.string.ADAS_enabled))
				.setSmallIcon(R.mipmap.ic_launcher)
				.setContentIntent(
						PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0)
				)
				.build();
		startForeground(SERVICE_NOTIFICATION_ID, notification);
		isRunning = true;
		startThread();
		enableDriveAssist = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(getString(R.string.enable_drive_assist), true);
		if (enableDriveAssist) {
			imageProcessor.initialize(this);
			driveAssistant.initialize(this);
		}
		dashCam.initialize();
		dataRecorder.initialize();
		dashCam.startRecording();
		dataRecorder.startRecording();
		if (enableDriveAssist) {
			driveAssistant.activate();
			imageProcessor.start();
		}
		Toast.makeText(getApplicationContext(), R.string.ADAS_enabled, Toast.LENGTH_SHORT).show();
		return START_REDELIVER_INTENT;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		dashCam.stopRecording();
		dataRecorder.stopRecording();
		if (enableDriveAssist) {
			imageProcessor.stop();
			driveAssistant.exit();
		}
		Toast.makeText(getApplicationContext(), R.string.ADAS_disabled, Toast.LENGTH_SHORT).show();
		stopThread();
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
}
