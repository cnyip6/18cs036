/*
 * Copyright (c) 2019. Yip Chung Nin
 * Developed as part of project 18CS036 (DashDroid - Dash Cam with driver assistance) at City University of Hong Kong
 * All rights reserved
 */

package p18cs036.dashdroid;

import android.util.Log;

import org.opencv.core.Mat;

import java.util.concurrent.ConcurrentHashMap;

import p18cs036.dashdroid.computervision.model.DetectedObject;
import p18cs036.dashdroid.computervision.model.TrafficLight;
import p18cs036.dashdroid.computervision.model.TrafficSign;
import p18cs036.dashdroid.computervision.model.Vehicle;
import p18cs036.dashdroid.computervision.processors.LaneStateDetector;

/**
 * Created on 10/12/2018.
 */
public class ConcurrentKeyValueMap extends ConcurrentHashMap<String, Object> {
	private final String LOG_TAG = this.getClass().getName();

	/**
	 * Constructs a new, empty KeyValueMap.
	 */
	public ConcurrentKeyValueMap() {
		super();
	}

	/**
	 * Constructs a KeyValueMap containing a copy of the mappings from the given
	 * KeyValueMap.
	 *
	 * @param map a KeyValueMap to be copied.
	 */
	public ConcurrentKeyValueMap(ConcurrentKeyValueMap map) {
		super(map);
	}


	/**
	 * Inserts an int value into the mapping of this <code>KeyValueMap</code>, replacing
	 * any existing value for the given key.
	 *
	 * @param key   a String
	 * @param value an int
	 */
	public void putInt(String key, int value) {
		put(key, value);
	}

	/**
	 * Returns the value associated with the given key, or null if
	 * no mapping of the desired type exists for the given key.
	 *
	 * @param key a String
	 * @return an int value
	 */
	public Integer getInt(String key) {
		return getInt(key, null);
	}

	/**
	 * Returns the value associated with the given key, or defaultValue if
	 * no mapping of the desired type exists for the given key.
	 *
	 * @param key          a String
	 * @param defaultValue Value to return if key does not exist
	 * @return an int value
	 */
	public Integer getInt(String key, Integer defaultValue) {
		Object o = get(key);
		if (o == null) {
			return defaultValue;
		}
		try {
			return (Integer) o;
		} catch (ClassCastException e) {
			typeWarning(key, o, "Integer", defaultValue, e);
			return defaultValue;
		}
	}

	/**
	 * Inserts a double value into the mapping of this <code>KeyValueMap</code>, replacing
	 * any existing value for the given key.
	 *
	 * @param key   a String
	 * @param value a double
	 */
	public void putDouble(String key, double value) {
		put(key, value);
	}

	/**
	 * Returns the value associated with the given key, or null if
	 * no mapping of the desired type exists for the given key.
	 *
	 * @param key a String
	 * @return a double value
	 */
	public Double getDouble(String key) {
		return getDouble(key, null);
	}

	/**
	 * Returns the value associated with the given key, or defaultValue if
	 * no mapping of the desired type exists for the given key.
	 *
	 * @param key          a String
	 * @param defaultValue Value to return if key does not exist
	 * @return a double value
	 */
	public Double getDouble(String key, Double defaultValue) {
		Object o = get(key);
		if (o == null) {
			return defaultValue;
		}
		try {
			return (Double) o;
		} catch (ClassCastException e) {
			typeWarning(key, o, "Double", defaultValue, e);
			return defaultValue;
		}
	}

	/**
	 * Inserts a Boolean value into the mapping, replacing
	 * any existing value for the given key.
	 *
	 * @param key   a String
	 * @param value a boolean
	 */
	public void putBoolean(String key, boolean value) {
		put(key, value);
	}

	/**
	 * Returns the value associated with the given key, or null if
	 * no mapping of the desired type exists for the given key.
	 *
	 * @param key a String
	 * @return a boolean value
	 */
	public Boolean getBoolean(String key) {
		return getBoolean(key, null);
	}

	/**
	 * Returns the value associated with the given key, or defaultValue if
	 * no mapping of the desired type exists for the given key.
	 *
	 * @param key          a String
	 * @param defaultValue Value to return if key does not exist
	 * @return a boolean value
	 */
	public Boolean getBoolean(String key, Boolean defaultValue) {
		Object o = get(key);
		if (o == null) {
			return defaultValue;
		}
		try {
			return (Boolean) o;
		} catch (ClassCastException e) {
			typeWarning(key, o, "Boolean", defaultValue, e);
			return defaultValue;
		}
	}


	/**
	 * Inserts a Mat into the mapping of this <code>KeyValueMap</code>, replacing
	 * any existing value for the given key.  Either key or value may be null.
	 *
	 * @param key   a String
	 * @param value a Mat object
	 */
	public void putMat(String key, Mat value) {
		put(key, value);
	}

	/**
	 * Returns the value associated with the given key, or null if
	 * no mapping of the desired type exists for the given key or a null
	 * value is explicitly associated with the key.
	 *
	 * @param key a String
	 * @return a Vehicle[] value, or null
	 */
	public Mat getMat(String key) {
		Object o = get(key);
		if (o == null) {
			return null;
		}
		try {
			return (Mat) o;
		} catch (ClassCastException e) {
			typeWarning(key, o, "Mat", e);
			return null;
		}
	}

	/**
	 * Inserts an <code>ArrayList<DetectedObject></code> into the mapping of this <code>KeyValueMap</code>, replacing
	 * any existing value for the given key.  Either key or value may be null.
	 *
	 * @param key   a String
	 * @param value an ArrayList<String> object, or null
	 */
	public void putDetectedObjectArray(String key, DetectedObject[] value) {
		put(key, value);
	}

	/**
	 * Returns the value associated with the given key, or null if
	 * no mapping of the desired type exists for the given key or a null
	 * value is explicitly associated with the key.
	 *
	 * @param key a String
	 * @return an ArrayList<String> value, or null
	 */
	public DetectedObject[] getDetectedObjectArray(String key) {
		Object o = get(key);
		if (o == null) {
			return null;
		}
		try {
			return (DetectedObject[]) o;
		} catch (ClassCastException e) {
			typeWarning(key, o, "ArrayList<String>", e);
			return null;
		}
	}

	/**
	 * Inserts a Vehicle array value into the mapping of this <code>KeyValueMap</code>, replacing
	 * any existing value for the given key.  Either key or value may be null.
	 *
	 * @param key   a String
	 * @param value a long array object, or null
	 */
	public void putDetectedVehicleArray(String key, Vehicle[] value) {
		put(key, value);
	}

	/**
	 * Returns the value associated with the given key, or null if
	 * no mapping of the desired type exists for the given key or a null
	 * value is explicitly associated with the key.
	 *
	 * @param key a String
	 * @return a Vehicle[] value, or null
	 */
	public Vehicle[] getDetectedVehicleArray(String key) {
		Object o = get(key);
		if (o == null) {
			return null;
		}
		try {
			return (Vehicle[]) o;
		} catch (ClassCastException e) {
			typeWarning(key, o, "Vehicle[]", e);
			return null;
		}
	}


	/**
	 * Inserts a Vehicle array value into the mapping of this <code>KeyValueMap</code>, replacing
	 * any existing value for the given key.  Either key or value may be null.
	 *
	 * @param key   a String
	 * @param value a long array object, or null
	 */
	public void putDetectedLightsArray(String key, TrafficLight[] value) {
		put(key, value);
	}

	/**
	 * Returns the value associated with the given key, or null if
	 * no mapping of the desired type exists for the given key or a null
	 * value is explicitly associated with the key.
	 *
	 * @param key a String
	 * @return a Vehicle[] value, or null
	 */
	public TrafficLight[] getDetectedLightsArray(String key) {
		Object o = get(key);
		if (o == null) {
			return null;
		}
		try {
			return (TrafficLight[]) o;
		} catch (ClassCastException e) {
			typeWarning(key, o, "TrafficLight[]", e);
			return null;
		}
	}


	/**
	 * Inserts a Vehicle array value into the mapping of this <code>KeyValueMap</code>, replacing
	 * any existing value for the given key.  Either key or value may be null.
	 *
	 * @param key   a String
	 * @param value a long array object, or null
	 */
	public void putDetectedSignArray(String key, TrafficSign[] value) {
		put(key, value);
	}

	/**
	 * Returns the value associated with the given key, or null if
	 * no mapping of the desired type exists for the given key or a null
	 * value is explicitly associated with the key.
	 *
	 * @param key a String
	 * @return a Vehicle[] value, or null
	 */
	public TrafficSign[] getDetectedSignArray(String key) {
		Object o = get(key);
		if (o == null) {
			return null;
		}
		try {
			return (TrafficSign[]) o;
		} catch (ClassCastException e) {
			typeWarning(key, o, "TrafficSign[]", e);
			return null;
		}
	}

	/**
	 * Inserts a value into the mapping of this <code>KeyValueMap</code>, replacing
	 * any existing value for the given key.  Either key or value may be null.
	 *
	 * @param key   a String
	 * @param value a String, or null
	 */
	public void putLaneStatus(String key, LaneStateDetector.LaneDepartureStatus value) {
		put(key, value);
	}

	/**
	 * Returns the value associated with the given key, or null if
	 * no mapping of the desired type exists for the given key or a null
	 * value is explicitly associated with the key.
	 *
	 * @param key a String
	 * @return a String value, or null
	 */

	public LaneStateDetector.LaneDepartureStatus getLaneStatus(String key) {
		final Object o = get(key);
		try {
			return (LaneStateDetector.LaneDepartureStatus) o;
		} catch (ClassCastException e) {
			typeWarning(key, o, "LanDepartureStatus", e);
			return null;
		}
	}


	private void typeWarning(String key, Object value, String className,
	                         Object defaultValue, ClassCastException e) {
		StringBuilder sb = new StringBuilder();
		sb.append("Key ");
		sb.append(key);
		sb.append(" expected ");
		sb.append(className);
		sb.append(" but value was a ");
		sb.append(value.getClass().getName());
		sb.append(".  The default value ");
		sb.append(defaultValue);
		sb.append(" was returned.");
		Log.w(LOG_TAG, sb.toString());
		Log.w(LOG_TAG, "Attempt to cast generated internal exception:");
		Log.w(LOG_TAG, e);
	}

	private void typeWarning(String key, Object value, String className,
	                         ClassCastException e) {
		typeWarning(key, value, className, "<null>", e);
	}
}
