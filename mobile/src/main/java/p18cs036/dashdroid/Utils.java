/*
 * Copyright (c) 2019. Yip Chung Nin
 * Developed as part of project 18CS036 (DashDroid - Dash Cam with driver assistance) at City University of Hong Kong
 * All rights reserved
 */

package p18cs036.dashdroid;

import android.content.Context;
import android.content.pm.PackageManager;
import android.media.Image;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Utils {

	public static boolean permissionsGranted(Context context, String[] perissions) {
		for (String perission : perissions) {
			if (ActivityCompat.checkSelfPermission(context, perission) != PackageManager.PERMISSION_GRANTED) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Concatenate the content of a string list into a string, using a deliiter to separate each item.<br/>
	 * This is for Java 7 and below. For Java 8 and up, use <code>String.joint()</code>.
	 *
	 * @param list      list of string
	 * @param delimiter delimiter to use in the resulted string
	 * @return the combined string
	 */
	public static String jointStringListToString(List<String> list, String delimiter) {
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < list.size(); i++) {
			if (i > 0) {
				stringBuilder.append(delimiter);
			}
			stringBuilder.append(list.get(i));
		}
		return stringBuilder.toString();
	}

	/**
	 * Write a file into storage.
	 *
	 * @param data        data to be written into file
	 * @param outDir      Directory to write to. Can be null in case of using absolute file path.
	 * @param outFileName Name of the output file, or an absolute file path.
	 * @return The created file.
	 */
	public static File writeFile(byte[] data, String outDir, String outFileName) throws IOException {
		File outPath = null;
		if (outDir != null) {
			outPath = new File(outDir);
			if (! outPath.exists()) {
				outPath.mkdir();
			}
		}
		File outFile = new File(outPath, outFileName);
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(outFile);
			out.write(data);
		} finally {
			if (out != null) {
				out.close();
			}
		}
		return outFile;
	}

	/**
	 * Write a text file into storage. This method is basically a wrapper method for  {@link #writeFile(byte[], String, String)} while converting <code>String</code> to <code>byte[]</code>.
	 *
	 * @param text        text to be written into file
	 * @param outDir      Directory to write to. Can be null in case of using absolute file path.
	 * @param outFileName Name of the output file, or an absolute file path.
	 * @return The created file.
	 * @see #writeFile(byte[], String, String)
	 */
	public static File writeTextFile(String text, String outDir, String outFileName) throws IOException {
		return writeFile(text.getBytes(), outDir, outFileName);
	}

	/**
	 * Wrapper method for {@link #writeTextFile(String, String, String)} , with additional precautions for external storage.
	 *
	 * @param text        text to be written into file
	 * @param outDir      Directory to write to. Can be null in case of using absolute file path.
	 * @param outFileName Name of the output file, or an absolute file path.
	 * @return The created file
	 * @see #writeTextFile(String, String, String)
	 */
	public static File writeTextFileToExternalStorage(String text, String outDir, String outFileName) throws IOException {
		if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
			return writeTextFile(text, outDir, outFileName);
		} else {
			throw new IOException("External storage not mounted");
		}
	}

	/**
	 * Copy a <code>InputStream</code> to a file.
	 *
	 * @param inStream    incoing stream
	 * @param outDir      Directory to write to. Can be null in case of using absolute file path.
	 * @param outFileName Name of the output file, or an absolute file path.
	 * @return The copied file
	 */
	public static File inputStreamToFile(InputStream inStream, String outDir, String outFileName) throws IOException {
		File outPath = null;
		if (outDir != null) {
			outPath = new File(outDir);
			if (! outPath.exists()) {
				outPath.mkdir();
			}
		}
		File outFile = new File(outPath, outFileName);
		if (inStream != null) {
			FileOutputStream out = null;
			try {
				out = new FileOutputStream(outFile);
				byte[] buffer = new byte[4 * 2014];
				int len;
				while ((len = inStream.read(buffer)) > 0) {
					out.write(buffer, 0, len);
				}
			} finally {
				if (out != null) {
					out.close();
				}
			}
			return outFile;
		} else {
			throw new NullPointerException("Source file does not exist");
		}
	}


	/**
	 * Retrieve a timestamp in a human readable format of the current time, in format of <code>yyyy-MM-dd'T'HH:mm:ss.SSSXXX</code>.
	 *
	 * @return string
	 */
	public static String getReadableTimestamp() {
		String timeFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZ";
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
			timeFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
		}
		return new SimpleDateFormat(timeFormat).format(new Date(System.currentTimeMillis()));
	}

	public static Mat imageToRGBMat(Image image) {
		if (image != null) {
			byte[] imageData;
			ByteBuffer yBuffer = image.getPlanes()[0].getBuffer();
			ByteBuffer uBuffer = image.getPlanes()[1].getBuffer();
			ByteBuffer vBuffer = image.getPlanes()[2].getBuffer();
			int ySize = yBuffer.remaining();
			int uSize = uBuffer.remaining();
			int vSize = vBuffer.remaining();
			imageData = new byte[ySize + uSize + vSize];
			//U and V are swapped
			yBuffer.get(imageData, 0, ySize);
			vBuffer.get(imageData, ySize, vSize);
			uBuffer.get(imageData, ySize + vSize, uSize);
			Mat mYuv = new Mat(image.getHeight() + image.getHeight() / 2, image.getWidth(), CvType.CV_8UC1);
			mYuv.put(0, 0, imageData);
			Mat mRGB = new Mat();
			Imgproc.cvtColor(mYuv, mRGB, Imgproc.COLOR_YUV2RGB_NV21, 3);
			return mRGB;
		}
		return null;
	}

	public static double meterPerSecondToKilometerPerHour(double ms) {
		return (ms * 60 * 60) / 1000;
	}

	/**
	 * Determine if a value is in a given range (inclusive).
	 *
	 * @param value   value to be examined.
	 * @param minimum Minimum allowed value
	 * @param maximum Maximum allowed value
	 * @return boolean
	 */
	public static boolean inRange(double value, double minimum, double maximum) {
		return value >= minimum && value <= maximum;
	}

	/**
	 * Constrict a value into a given range.
	 *
	 * @param value   value to be examined.
	 * @param minimum Minimum allowed value
	 * @param maximum Maximum allowed value
	 * @return A int with the given value. if the value is outside the given range, it will be clamped into the minimum or maximum of the given range.
	 */
	public static double clampValue(double value, double minimum, double maximum) {
		return Math.max(minimum, Math.min(maximum, value));
	}

	/**
	 * Constrict a value into a given range. <br>
	 * <code>int</code> wrapper for {@link #clampValue(double, double, double)}.
	 *
	 * @param value   value to be examined.
	 * @param minimum Minimum allowed value
	 * @param maximum Maximum allowed value
	 * @return A int with the given value. if the value is outside the given range, it will be clamped into the minimum or maximum of the given range.
	 * @see #clampValue(double, double, double)
	 */
	public static int clampValueToInt(double value, double minimum, double maximum) {
		return (int) clampValue(value, minimum, maximum);
	}

	/**
	 * Copy a file from asset folder to the storage.
	 *
	 * @param context  Application context.
	 * @param fileName File name in asset folder
	 * @param outDir   Directory to write to. Can be null in case of using absolute file path.
	 * @return The created file
	 */
	public static File copyFileFromAsset(Context context, String fileName, String outDir) throws IOException {
		return inputStreamToFile(context.getAssets().open(fileName), outDir, fileName);
	}

	/**
	 * Copy a file from asset folder to the default app internal storage.
	 *
	 * @param context  Application context.
	 * @param fileName File name in asset folder
	 * @return The created file
	 */
	public static File copyFileFromAssetToInternalStorage(Context context, String fileName, String outDirName) throws IOException {
		return copyFileFromAsset(context, fileName, context.getDir(outDirName, Context.MODE_PRIVATE).getAbsolutePath());
	}

	/**
	 * Determine whether a string is a valid integer.
	 *
	 * @param s string to be parsed
	 * @return boolean
	 */
	public static boolean isInteger(String s) {
		return isInteger(s, 10);
	}

	/**
	 * Determine whether a string is a valid integer under a particular radix.
	 *
	 * @param s     string to be parsed
	 * @param radix radix of the supposed integer
	 * @return boolean
	 */
	public static boolean isInteger(String s, int radix) {
		if (s == null || s.isEmpty()) {
			return false;
		}
		for (int i = 0; i < s.length(); i++) {
			if (i == 0 && s.charAt(i) == '-') {
				if (s.length() == 1) {
					return false;
				}
			} else if (Character.digit(s.charAt(i), radix) < 0) {
				return false;
			}
		}
		return true;
	}

	public static double meterPerSecondToKilometersPerHour(double mps) {
		return (mps * 60 * 60) / 1000;
	}

	/**
	 * Applies a <i>Simple Moving Average Filter</i> on the given list of continuous data. The size of the filter is scaled automatically to match the size of the data. Otherwise, it behaves like any other Moving Average implementation.
	 *
	 * @param data          Data to be filtered. The value contained in the list should be continuous, in chronological order.
	 * @param maxFilterSize Size of the filter
	 * @param <T>           Any data type thant extends the Java <code>Number</code> class
	 * @return The averaged value.
	 */
	public static <T extends Number> Double simpleMovingAverageFilter(List<T> data, int maxFilterSize) {
		Double mean = null;
		if (data == null || data.size() <= 0) {
			return null;
		} else if (data.size() == 1) {
			return data.get(0).doubleValue();
		} else {
			while (maxFilterSize > 0 && mean == null) {
				if (maxFilterSize > data.size()) {
					maxFilterSize--;
				} else {
					double sum = 0;
					for (int i = 1; i <= maxFilterSize; i++) {
						sum += data.get(data.size() - i).doubleValue();
					}
					mean = (sum / maxFilterSize);
				}
			}
		}
		return mean;
	}

	/**
	 * Applies a <i>Low-pass filter</i> on the given list of continuous data.
	 *
	 * @param data            Data to be filtered. The value contained in the list should be continuous, in chronological order.
	 * @param maxSampleSize   Max sample size used for filtering
	 * @param smoothingFactor smoothing factor alpha
	 * @param <T>             Any data type thant extends the Java <code>Number</code> class
	 * @return The averaged value.
	 */
	public static <T extends Number> Double lowPassFilter(List<T> data, int maxSampleSize, double smoothingFactor) {
		if (data == null || data.size() <= 0) {
			return null;
		} else {
			if (smoothingFactor < 1) {
				smoothingFactor = 1;
			}
			double value = data.get(0).doubleValue();
			if (maxSampleSize >= data.size()) {
				for (int i = 1; i < data.size(); i++) {
					value += (data.get(i).doubleValue() - value) / smoothingFactor;
				}
			} else {
				value = data.get(data.size() - maxSampleSize).doubleValue();
				for (int i = data.size() - maxSampleSize + 1; i < data.size(); i++) {
					value += (data.get(i).doubleValue() - value) / smoothingFactor;
				}
			}
			return value;
		}

	}

	public static double invertSign(double value) {
		return 0 - value;
	}

	public static <T> T getLastElementOfList(List<T> list) {
		if (list == null || list.isEmpty()) {
			return null;
		}
		return list.get(list.size() - 1);
	}

	public static <T> T getFirstElementOfList(List<T> list) {
		if (list == null || list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}

}
