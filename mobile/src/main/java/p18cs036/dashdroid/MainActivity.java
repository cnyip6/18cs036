/*
 * Copyright (c) 2019. Yip Chung Nin
 * Developed as part of project 18CS036 (DashDroid - Dash Cam with driver assistance) at City University of Hong Kong
 * All rights reserved
 */

package p18cs036.dashdroid;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {
	private final String LOG_TAG = this.getClass().getName();

	private static final int PERMISSION_REQUEST_CODE = 1;

	//UI components
	private ToggleButton mainSwitch;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mainSwitch = findViewById(R.id.main_switch);
		mainSwitch.setChecked(DashDroidService.isRunning);
		mainSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					startADASService();
				} else {
					stopADASService();
				}
			}
		});
	}

	private void startADASService() {
		String[] permissions = new String[]{
				Manifest.permission.CAMERA,
				Manifest.permission.RECORD_AUDIO,
				Manifest.permission.ACCESS_FINE_LOCATION,
				Manifest.permission.WRITE_EXTERNAL_STORAGE
		};
		if (! Utils.permissionsGranted(this, permissions)) {
			ActivityCompat.requestPermissions(this, permissions, PERMISSION_REQUEST_CODE);
		} else {
			startService(new Intent(this, DashDroidService.class));
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] results) {
		switch (requestCode) {
			case PERMISSION_REQUEST_CODE: {
				if (results.length > 0 && results[0] == PackageManager.PERMISSION_GRANTED) {
					startADASService();
				} else {
					finish();
					//TODO tell user to quit
				}
			}
		}
	}

	private void stopADASService() {
		stopService(new Intent(this, DashDroidService.class));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_main_activity, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_settings:
				startActivity(new Intent(this, SettingsActivity.class));
				break;
			default:
				//nothing
		}


		return super.onOptionsItemSelected(item);
	}

}
