/*
 * Copyright (c) 2019. Yip Chung Nin
 * Developed as part of project 18CS036 (DashDroid - Dash Cam with driver assistance) at City University of Hong Kong
 * All rights reserved
 */

package p18cs036.dashdroid;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.HandlerThread;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import com.googlecode.tesseract.android.TessBaseAPI;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import p18cs036.dashdroid.computervision.ImageProcessingManager;
import p18cs036.dashdroid.computervision.model.TrafficLight;
import p18cs036.dashdroid.computervision.model.TrafficSign;
import p18cs036.dashdroid.computervision.model.Vehicle;
import p18cs036.dashdroid.computervision.processors.IlluminationFinder;
import p18cs036.dashdroid.computervision.processors.LaneStateDetector;
import p18cs036.dashdroid.computervision.processors.ObjectDetector;
import p18cs036.dashdroid.computervision.processors.VehicleTracker;
import p18cs036.dashdroid.dashcam.DataRecorder;

public class DriveAssistant {
	private final String LOG_TAG = this.getClass().getName();

	private static DriveAssistant ourInstance = new DriveAssistant();

	private static final int INFO_UPDATE_RATE = 10;

	private static final int URGENT_WARNING_INTERVAL = 1500;
	private static final int NON_URGENT_WARNING_INTERVAL = 10 * 1000;

	private static final double SAFE_DISTANCE_FORWARD_WIDTH = LaneStateDetector.LANE_WIDTH;//unit meters
	private static final int SAFE_DISTANCE_FACTOR = 2;

	private static final double COLLISION_WARNING_DISTANCE_FACTOR = SAFE_DISTANCE_FACTOR + 1.5;
	private static final int COLLISION_WARNING_IGNORE_THRESHOLD = 60;
	private static final int COLLISION_WARNING_MIN_DISTANCE = 10;
	private static final double COLLISION_WARNING_WIDTH = 3;

	private static final int LANE_DEPARTURE_WARNING_TURNRATE = 25;//unit deg per sec
	private static final int LANE_DEPARTURE_WARNING_ERROR_TOLERANCE = 0;

	private static final int SPEED_STOPPED_THRESHOLD = 1;//speed lower than this is considered stopped.

	//own thread for running repeating stuff
	private HandlerThread thread;
	private Handler handler;

	private Context context;

	private ConcurrentKeyValueMap processingResultsMap;

	//turnrate calculation
	private double turnRate = 0;
	private ArrayList<Double> pastHeading;
	private ArrayList<Double> pastAvgHeading;

	private static final int TURN_RATE_FILTER_SIZE = 30;

	//illumination
	private boolean poorIllumination = false;

	//lane departure record
	private LaneStateDetector.LaneDepartureStatus status = null;
	private boolean inLane = false;
	private int inLaneCount = 0;
	private int noLaneCount = 0;
	private int inLaneConsectCount = 0;
	private int offLaneConsectCount = 0;
	private int noLaneConsectCount = 0;

	//known vehicles
	private Vehicle[] vehicles;

	//known traffic lights
	private TrafficLight[] trafficLights;
	private int trafficLightCount = 0;

	//ocr for traffic signs
	private static final String OCR_DATA_FILE_NAME = "eng.traineddata";
	private TessBaseAPI tesseract;

	//speedlimit and stuff
	private static final int MAX_POSSIBLE_SPEED = 150;
	private double speed;
	private int speedLimit = 40;//TODO load default speed form setting

	//notification
	private TextToSpeech tts;

	//feature toggles
	private boolean lowLightWarn;
	private boolean collideWarn;
	private boolean safeDistanceWarn;
	private boolean laneWarn;
	private boolean lightWarn;
	private boolean speedWarn;

	private enum Notification {//TODO revise content text
		LOW_ILLUMINATION("Illumination poor. Please switch on headlight."),
		GPS_FAIL("GPS not available. Collision warning and speed limit warning will not be functional."),
		NEW_SPEED_LIMIT("Speed limit detected, current speed limit"),
		OVER_SPEED("You are over speed limit."),
		TRAFFIC_LIGHT("Traffic light ahead"),
		SAFE_DISTANCE("Please maintain distance with forward vehicles"),
		STOP("Please stop."),
		COLLISION("COLLISION"),
		MOVE("Please move"),
		OBSTRUCTION("Obstruction Ahead."),
		OFF_LANE("You are out of lane."),
		START("Drive assistance enabled."),
		END("Drive assistance disabled."),
		TESTING("Testing of Text to Speech Service");

		private final String message;

		Notification(final String message) {
			this.message = message;
		}

		public String getMessage() {
			return message;
		}
	}

	private HashMap<Notification, Long> lastNotifyTime;

	private DriveAssistant() {
	}

	public static DriveAssistant getInstance() {
		return ourInstance;
	}

	private void startThread() {
		thread = new HandlerThread("DriveAssistThread");
		thread.start();
		handler = new Handler(thread.getLooper());
	}

	private void stopThread() {
		handler.removeCallbacksAndMessages(null);
		thread.quitSafely();
		try {
			thread.join();
			thread = null;
			handler = null;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void initialize(Context context) {
		this.context = context;
		processingResultsMap = ImageProcessingManager.getInstance().getResultsMap();
		lastNotifyTime = new HashMap<>();
		pastHeading = new ArrayList<>();
		pastAvgHeading = new ArrayList<>();
		startThread();
		tts = new TextToSpeech(context, new TextToSpeech.OnInitListener() {
			@Override
			public void onInit(int status) {
				if (status == TextToSpeech.SUCCESS) {
					int result = tts.setLanguage(Locale.US);
					if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
						Log.e(LOG_TAG, "TTS language not supported");
					} else {
						issueWarning(Notification.START, true);
					}
				} else {
					Log.e(LOG_TAG, "Fail to initialize TTS");
				}
			}
		});
		tesseract = new TessBaseAPI();
		try {
			String pDir = context.getDir("ocr", Context.MODE_PRIVATE).getAbsolutePath();
			String dir = pDir + "/tessdata";
			Utils.copyFileFromAsset(context, OCR_DATA_FILE_NAME, dir);
			tesseract.init(pDir, "eng");
			tesseract.setVariable(TessBaseAPI.VAR_CHAR_BLACKLIST, "!?@#$%&*()<>_-+=/:;'\"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,");
			tesseract.setVariable(TessBaseAPI.VAR_CHAR_WHITELIST, "0123456789");
			tesseract.setVariable("classify_bln_numeric_mode", "1");
		} catch (Exception e) {
			Log.e(LOG_TAG, "Fail to load OCR data", e);
		}
		vehicles = new Vehicle[0];
		trafficLights = new TrafficLight[0];
		//see if features are enabled
		lowLightWarn = PreferenceManager.getDefaultSharedPreferences(context).getBoolean(context.getString(R.string.enable_illu_warn), true);
		collideWarn = PreferenceManager.getDefaultSharedPreferences(context).getBoolean(context.getString(R.string.enable_collision_warning), true);
		safeDistanceWarn = PreferenceManager.getDefaultSharedPreferences(context).getBoolean(context.getString(R.string.enable_safety_distance_warning), true);
		laneWarn = PreferenceManager.getDefaultSharedPreferences(context).getBoolean(context.getString(R.string.enable_lane_warn), true);
		lightWarn = PreferenceManager.getDefaultSharedPreferences(context).getBoolean(context.getString(R.string.enable_light_warn), true);
		speedWarn = PreferenceManager.getDefaultSharedPreferences(context).getBoolean(context.getString(R.string.enable_speed_warn), true);


	}

	public void activate() {
		handler.post(new Runnable() {
			@Override
			public void run() {
				Log.v(LOG_TAG, "Updating");
				if (collideWarn || safeDistanceWarn) {
					for (Vehicle v : vehicles) {
						v.updateRange();
					}
					if (collideWarn) {
						checkForCollisionWarning();
					}
					if (safeDistanceWarn) {
						checkForSafeDistanceWarning();
					}
				}
				if (laneWarn) {
					updateTurnRate();
					checkForLaneDeparture();
				}
				if (lightWarn) {
					checkForTrafficLight();
				}
				if (speedWarn) {
					checkForSpeedLimit();
				}
				handler.postDelayed(this, 1000 / INFO_UPDATE_RATE);
			}
		});
	}

	private void updateTurnRate() {
		Double heading = processingResultsMap.getDouble(DataRecorder.DATA_KEY_HEADING);
		if (heading != null) {
			pastHeading.add(heading);
			double avgHeading = Utils.simpleMovingAverageFilter(pastHeading, TURN_RATE_FILTER_SIZE);
			pastAvgHeading.add(avgHeading);
			if (pastAvgHeading.size() > INFO_UPDATE_RATE) {
				turnRate = Math.abs(Utils.getLastElementOfList(pastAvgHeading) - pastAvgHeading.get(pastAvgHeading.size() - INFO_UPDATE_RATE));
			} else {
				turnRate = Math.abs(Utils.getFirstElementOfList(pastAvgHeading) - avgHeading) * INFO_UPDATE_RATE;
			}
		}
	}

	private void updateVehicles() {
		handler.post(new Runnable() {
			@Override
			public void run() {
				Log.v(LOG_TAG, "Updating vehicle info");
				vehicles = processingResultsMap.getDetectedVehicleArray(VehicleTracker.KEY);
			}
		});
	}

	private void updateTrafficLights() {
		handler.post(new Runnable() {
			@Override
			public void run() {
				Log.v(LOG_TAG, "Updating traffic light info");
				TrafficLight[] lights = processingResultsMap.getDetectedLightsArray(ObjectDetector.KEY_TRAFFIC_LIGHTS);
				if (lights != null) {
					trafficLights = lights;
				}
			}
		});
	}

	private void readRoadSigns() {
		handler.post(new Runnable() {
			@Override
			public void run() {
				TrafficSign[] trafficSigns = processingResultsMap.getDetectedSignArray(ObjectDetector.KEY_TRAFFIC_SIGNS);
				for (TrafficSign s : trafficSigns) {
					Mat image = s.getImage();
					Mat gray = new Mat();
					Mat binary = new Mat();
					Imgproc.cvtColor(image, gray, Imgproc.COLOR_RGB2GRAY);
					Imgproc.threshold(gray, binary, 0, 255, Imgproc.THRESH_OTSU | Imgproc.THRESH_BINARY);
					Bitmap bm = Bitmap.createBitmap(image.cols(), image.rows(), Bitmap.Config.ARGB_8888);
					org.opencv.android.Utils.matToBitmap(binary, bm);
					tesseract.setImage(bm);
					String content = tesseract.getUTF8Text();
					if (Utils.isInteger(content)) {
						//is this a speed sign?
						int spdLmt = Integer.parseInt(content);
						if (Utils.inRange(spdLmt, 1, MAX_POSSIBLE_SPEED) && spdLmt % 5 == 0) {//TODO is mod 5 necessary?
							speedLimit = spdLmt;
							if (speedWarn) {
								issueWarning(Notification.NEW_SPEED_LIMIT, false);
							}
						}
					}
				}
			}
		});
	}

	public void onNewVehicleData() {
		updateVehicles();
	}

	public void onNewTrafficLightData() {
		updateTrafficLights();
	}

	public void onNewRoadSignData() {
		readRoadSigns();
	}

	public void onLaneStatusUpdated() {
		LaneStateDetector.LaneDepartureStatus newStatus = processingResultsMap.getLaneStatus(LaneStateDetector.KEY);
		if (newStatus != null) {
			if (newStatus != status) {
				inLaneConsectCount = offLaneConsectCount = noLaneConsectCount = 0;
			}
			switch (newStatus) {
				case IN_LANE:
					inLaneConsectCount++;
					break;
				case OFF_LANE_LEFT:
				case OFF_LANE_RIGHT:
					offLaneConsectCount++;
				default:
					noLaneConsectCount++;
			}
		}
	}

	public void onIlluminationsUpdated() {
		handler.post(new Runnable() {
			@Override
			public void run() {

				boolean isDark = processingResultsMap.getBoolean(IlluminationFinder.KEY_ILLUMINATION, false);
				if (isDark && ! poorIllumination) {
					if (! processingResultsMap.getBoolean(IlluminationFinder.KEY_HEADLIGHTS_ENABLED)) {
						issueWarning(Notification.LOW_ILLUMINATION, false);
					}

				}
				poorIllumination = isDark;
			}
		});
	}

	private void checkForTrafficLight() {
		if (trafficLights.length != trafficLightCount) {
			if (trafficLights.length > trafficLightCount) {
				//okay, new lights up
				if (speed > SPEED_STOPPED_THRESHOLD) {
					//no need to give warning when stationary
					issueWarning(Notification.TRAFFIC_LIGHT, false);
				}
				Boolean stopSignal = null;
				for (TrafficLight light : trafficLights) {
					//see if all lights are in unison
					Boolean s = null;
					if (light.isRed()) {
						s = true;
					} else if (light.isGreen()) {
						s = false;
					}
					if (stopSignal != null && s != stopSignal) {
						stopSignal = null;
						break;
					} else {
						stopSignal = s;
					}

				}
				if (stopSignal != null) {
					if (stopSignal && speed > SPEED_STOPPED_THRESHOLD) {
						issueWarning(Notification.STOP, false);
					} else if (speed < SPEED_STOPPED_THRESHOLD) {
						issueWarning(Notification.MOVE, false);
					}
				}
			}
			trafficLightCount = trafficLights.length;
		}
	}

	private void checkForCollisionWarning() {
		for (Vehicle v : vehicles) {
			if (v.getRangeRate() != null) {
				double range = v.getRange();
				if (range <= COLLISION_WARNING_IGNORE_THRESHOLD) {
					double closeInRate = Utils.invertSign(v.getRangeRate()) * INFO_UPDATE_RATE;//range rate is negative while closing in, multiply to match m/s as unit
					if (closeInRate * COLLISION_WARNING_DISTANCE_FACTOR >= range) {
						if (v.getLateralDisplacement() > COLLISION_WARNING_WIDTH / 2) {
							issueWarning(Notification.COLLISION, true);
						}
					}
				}

			}
		}
	}

	private void checkForSafeDistanceWarning() {
		for (Vehicle v : vehicles) {
			if (v.getLateralDisplacement() != null && Math.abs(v.getLateralDisplacement()) <= SAFE_DISTANCE_FORWARD_WIDTH / 2) {
				if (v.getRange() < getSafeDistance()) {
					issueWarning(Notification.SAFE_DISTANCE, false);
				}
			}
		}
	}

	private void checkForLaneDeparture() {
		if (inLaneConsectCount > LANE_DEPARTURE_WARNING_ERROR_TOLERANCE) {
			inLane = true;
		} else if (offLaneConsectCount > LANE_DEPARTURE_WARNING_ERROR_TOLERANCE) {
			if (inLane) {
				if (turnRate > LANE_DEPARTURE_WARNING_TURNRATE) {
					boolean obstructed = false;
					for (Vehicle v : vehicles) {
						if (v.getLateralDisplacement() != null && Math.abs(v.getLateralDisplacement()) <= SAFE_DISTANCE_FORWARD_WIDTH / 2) {
							if (v.getRange() < LaneStateDetector.DETECT_RANGE) {
								obstructed = true;
							}
						}
					}
					if (! obstructed) {
						issueWarning(Notification.OFF_LANE, false);
					}
				}
			}
			inLane = false;
		} else if (noLaneConsectCount > LANE_DEPARTURE_WARNING_ERROR_TOLERANCE) {
			inLane = false;
		}
	}

	private double getSafeDistance() {
		double speed = processingResultsMap.getDouble(DataRecorder.DATA_KEY_SPEED);//unit should be in m/s
		return SAFE_DISTANCE_FACTOR * speed;
	}

	private void checkForSpeedLimit() {
		if (Utils.meterPerSecondToKilometersPerHour(speed) > speedLimit) {//TODO adjust KMH and MPH by user setting
			issueWarning(Notification.OVER_SPEED, true);
		}
	}

	public void warnNoGPS() {
		issueWarning(Notification.GPS_FAIL, false);
	}

	private void issueWarning(Notification notification, boolean urgent) {
		Log.i(LOG_TAG, "Issuing warning" + notification.name());
		if (lastNotifyTime.get(notification) == null) {
			lastNotifyTime.put(notification, 0L);
		}
		if (urgent && System.currentTimeMillis() - lastNotifyTime.get(notification) > URGENT_WARNING_INTERVAL) {
			tts.speak(notification.getMessage(), TextToSpeech.QUEUE_FLUSH, null, null);
			lastNotifyTime.put(notification, System.currentTimeMillis());
		} else if (System.currentTimeMillis() - lastNotifyTime.get(notification) > NON_URGENT_WARNING_INTERVAL) {
			if (notification == Notification.NEW_SPEED_LIMIT) {
				//TODO different unit
				tts.speak(notification.getMessage() + " " + speedLimit + "kilometers per hour", TextToSpeech.QUEUE_ADD, null, null);
			} else {
				tts.speak(notification.getMessage(), TextToSpeech.QUEUE_ADD, null, null);
			}
			lastNotifyTime.put(notification, System.currentTimeMillis());
		}

	}

	public void exit() {
		issueWarning(Notification.END, true);
		stopThread();
		if (tts != null) {
			tts.stop();
			tts.shutdown();
		}
	}
}
